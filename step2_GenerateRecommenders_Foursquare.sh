# !/bin/bash
# Necessary to execute with ./

: '
  Script for Foursquare dataset. It generates the recommenders and
  compute the evaluations
'

#Global variables for jvm
JAR=target/AttrEval4RecSys-0.2.1-SNAPSHOT.jar
jvm_memory=-Xmx30G
train_test_files=TrainTest
java_command=java


rec_prefix=rec
sim_prefix=sim


# General parameters of recommendations
items_recommended=100
rec_strats="T_I" # T_I is train items
train_test_files=TrainTest
dataset_speficic="FSNYFilterUsers2Core"
cutoffs="5,10,20"


original_datasets=OriginalDatasets
feature_file=$original_datasets/Foursquare_NY/NY_Features_LEVEL1.txt


# Specific parameters of recommendations
# k-NN approaches
all_neighbours="40 60 80 100 120"

# HKV
k_factors="10 50 100"
lambda_factorizers="0.1 1 10"
alpha_factorizers="0.1 1 10"


mymedialite_path=MyMediaLite-3.11/bin
bpr_factors=$k_factors
bpr_bias_regs="0 0.5 1"
bpr_learn_rate=0.05
bpr_iter=50
bpr_regs_u="0.0025 0.001 0.005 0.01 0.1"
extension_Mymed=MyMedLt




for rec_strat in $rec_strats
do
  for dataset in $dataset_speficic
  do
    train_file=$train_test_files"$dataset"/"$dataset"Rnd8020Train.txt
    test_file=$train_test_files"$dataset"/"$dataset"Rnd8020Test.txt

    recommendation_folder="RecommendationFolder""$dataset"
    mkdir -p $recommendation_folder

    #Generate sim file for jaccard and a CB sim
    for item_sim in SetJaccardItemSimilarity CBCosineItemSimilarity
    do
      $java_command $jvm_memory -jar $JAR -o ranksysSaveIBSimFile -trf $train_file -tsf $train_file -cIndex false -rs $item_sim -ff $feature_file -osf $train_test_files"$dataset"/"$item_sim".txt -cs 0
    done
    wait

    # Generate the CB-similarity with both sources of information
    alpha_fst_feature="0.8"
    alpha_snd_feature="0.6"
    file_feature_fst=NY_Features_LEVEL3.txt
    file_feature_snd=NY_Features_LEVEL1.txt

    output_sim_file=$train_test_files"$dataset"/SimComposedHierarchicalJaccardFeaturesL3L1Alphas"$alpha_fst_feature""$alpha_snd_feature".txt
    $java_command $jvm_memory -jar $JAR -o saveComposedSimilarity -trf $train_file -cs 0 -orf $output_sim_file -aFactorizer "$alpha_fst_feature","$alpha_snd_feature" -ff $original_datasets/Foursquare_NY/$file_feature_fst","$original_datasets/Foursquare_NY/$file_feature_snd

    # Now, each feature separated (directors and genres)
    alpha_fst_feature="0.8"
    file_feature_fst=NY_Features_LEVEL3.txt

    output_sim_file=$train_test_files"$dataset"/SimComposedHierarchicalJaccardFeaturesL3Alpha"$alpha_fst_feature".txt
    $java_command $jvm_memory -jar $JAR -o saveComposedSimilarity -trf $train_file -cs 0 -orf $output_sim_file -aFactorizer "$alpha_fst_feature" -ff $original_datasets/Foursquare_NY/$file_feature_fst

    alpha_snd_feature="0.6"
    file_feature_snd=NY_Features_LEVEL1.txt

    output_sim_file=$train_test_files"$dataset"/SimComposedHierarchicalJaccardFeaturesL1Alpha"$alpha_snd_feature".txt
    $java_command $jvm_memory -jar $JAR -o saveComposedSimilarity -trf $train_file -cs 0 -orf $output_sim_file -aFactorizer "$alpha_snd_feature" -ff $original_datasets/Foursquare_NY/$file_feature_snd


    output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_"skylineRelevanceRecommender"_"$rec_strat".txt
    $java_command $jvm_memory -jar $JAR -o skylineRecommenders -trf $train_file -tsf $test_file -cIndex true -rr "skylineRelevanceRecommender" -rs "notUsed" -nI $items_recommended -n 20 -orf $output_rec_file -recStrat $rec_strat -thr 1


    #Reverse
    output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_"skylineAntiRelevanceRecommender"_"$rec_strat".txt
    $java_command $jvm_memory -jar $JAR -o skylineRecommenders -trf $train_file -tsf $test_file -cIndex true -rr "skylineAntiRelevanceRecommender" -rs "notUsed" -nI $items_recommended -n 20 -orf $output_rec_file -recStrat $rec_strat -antiRelTh 2

    for ranksys_recommender in PopularityRecommender RandomRecommender
    do
      output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_"$ranksys_recommender"_"$rec_strat".txt
      $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex true -rr "$ranksys_recommender" -rs "notUsed" -nI $items_recommended -n 20 -orf $output_rec_file -recStrat $rec_strat
    done # End pop and Rnd
    wait


    # for all neighbours
    for neighbours in $all_neighbours
    do

      #Ranksys with similarities UserBased
      for UB_sim in SetJaccardUserSimilarity VectorCosineUserSimilarity
      do
        output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_UB_"$UB_sim"_k"$neighbours"_"$rec_strat".txt
        $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex false -rr UserNeighborhoodRecommender -rs $UB_sim -nI $items_recommended -n $neighbours -orf $output_rec_file -recStrat $rec_strat

      done # End UB sim
      wait

      for IB_sim in SetJaccardItemSimilarity VectorCosineItemSimilarity
      do
        output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_IB_"$IB_sim"_k"$neighbours"_"$rec_strat".txt
        $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex false -rr ItemNeighborhoodRecommender -rs $IB_sim -nI $items_recommended -n $neighbours -orf $output_rec_file -recStrat $rec_strat

      done
      wait #IB Sim

      output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_UBContentBasedRecommender_k"$neighbours"_"$rec_strat".txt
      $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex false -rr UBContentBasedRecommender -ff2 $feature_file -nI $items_recommended -n $neighbours -orf $output_rec_file -recStrat $rec_strat




    done # End neigh
    wait

    output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksysBaselineCB_"$rec_strat".txt
    $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex false -rr BaselineCB -ff2 $feature_file -nI $items_recommended -n $neighbours -orf $output_rec_file -recStrat $rec_strat

    for recommender in MFRecommenderHKV
    do
      for k_factor in $k_factors
      do
        for lambda_val in $lambda_factorizers
        do
          for alpha_val in $alpha_factorizers
          do
            #Neighbours is put to 20 because this recommender does not use it
            output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_"$recommender"_kFactor"$k_factor"_a"$alpha_val"_l"$lambda_val"_"$rec_strat".txt
            $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex false -rr $recommender -rs "notUsed" -nI $items_recommended -n 20 -orf $output_rec_file -kFactorizer $k_factor -aFactorizer $alpha_val -lFactorizer $lambda_val -recStrat $rec_strat
          done
          wait #End alpha values
        done
        wait #End lambda
      done
      wait #End KFactor
    done
    wait #End RankRecommender



    for repetition in 1 #2 3 4
    do
      for bpr_factor in $bpr_factors
      do
        for bpr_bias_reg in $bpr_bias_regs
        do
          for bpr_reg_U in $bpr_regs_u #Regularization for items and users is the same
          do
            bpr_reg_J=$(echo "$bpr_reg_U/10" | bc -l)

            output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_"$extension_Mymed"_BPRMF_nFact"$bpr_factor"_nIter"$bpr_iter"_LearnR"$bpr_learn_rate"_BiasR"$bpr_bias_reg"_RegU"$bpr_reg_U"_RegI"$bpr_reg_U"_RegJ"$bpr_reg_J""Rep$repetition"_"$rec_strat".txt
            echo $output_rec_file
            output_rec_file2=$output_rec_file"Aux".txt
            if [ ! -f *"$output_rec_file"* ]; then
              if [[ $rec_strat == *T_I* ]]; then
                echo "./$mymedialite_path/item_recommendation --training-file=$train_file --recommender=BPRMF --prediction-file=$output_rec_file2 --predict-items-number=$items_recommended --recommender-options="num_factors=$bpr_factor bias_reg=$bpr_bias_reg reg_u=$bpr_reg_U reg_i=$bpr_reg_U reg_j=$bpr_reg_J learn_rate=$bpr_learn_rate UniformUserSampling=false WithReplacement=false num_iter=$bpr_iter""
                ./$mymedialite_path/item_recommendation --training-file=$train_file --recommender=BPRMF --prediction-file=$output_rec_file2 --predict-items-number=$items_recommended --recommender-options="num_factors=$bpr_factor bias_reg=$bpr_bias_reg reg_u=$bpr_reg_U reg_i=$bpr_reg_U reg_j=$bpr_reg_J learn_rate=$bpr_learn_rate UniformUserSampling=false WithReplacement=false num_iter=$bpr_iter"
                $java_command $jvm_memory -jar $JAR -o ParseMyMediaLite -trf $output_rec_file2 $test_file $output_rec_file
                rm $output_rec_file2
              else
                echo "./$mymedialite_path/item_recommendation --training-file=$train_file --recommender=BPRMF --prediction-file=$output_rec_file2 --predict-items-number=$items_recommended --repeated-items --recommender-options="num_factors=$bpr_factor bias_reg=$bpr_bias_reg reg_u=$bpr_reg_U reg_i=$bpr_reg_U reg_j=$bpr_reg_J learn_rate=$bpr_learn_rate UniformUserSampling=false WithReplacement=false num_iter=$bpr_iter""
                ./$mymedialite_path/item_recommendation --training-file=$train_file --recommender=BPRMF --prediction-file=$output_rec_file2 --predict-items-number=$items_recommended --repeated-items --recommender-options="num_factors=$bpr_factor bias_reg=$bpr_bias_reg reg_u=$bpr_reg_U reg_i=$bpr_reg_U reg_j=$bpr_reg_J learn_rate=$bpr_learn_rate UniformUserSampling=false WithReplacement=false num_iter=$bpr_iter"
                $java_command $jvm_memory -jar $JAR -o ParseMyMediaLite -trf $output_rec_file2 $test_file $output_rec_file
                rm $output_rec_file2
              fi
            fi
          done # Reg U
          wait
        done # Bias reg
        wait
      done # Factors
      wait


    done # Repetition
    wait

  done # End train test files
  wait
done # End recommendationStrategy splits
wait


: '
Evaluation part
'
acc_prefix=naeval
evthreshold=1
for dataset in $dataset_speficic
do
  train_file=$train_test_files"$dataset"/"$dataset"Rnd8020Train.txt
  test_file=$train_test_files"$dataset"/"$dataset"Rnd8020Test.txt

  results_Folder=resultsFolder"$dataset"

  mkdir -p $results_Folder

  #Find all recommenders and compute the ranksysEvaluation

  recommendation_Folder=RecommendationFolder"$dataset"

  find $recommendation_Folder/ -name "$rec_prefix"* | while read recFile; do
    rec_FileName=$(basename "$recFile" .txt) #extension removed


    if [[ $rec_FileName == *_* ]]; then
      for evthreshold in 1 #we work with implicit data
      do

        # Normal evaluations. All users
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false
        fi

        # Non exact evaluation using features level 3 and level 1 composed
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"NonExactComposed.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -nonEM true -rs $train_test_files"$dataset"/SimComposedHierarchicalJaccardFeaturesL3L1Alphas0.80.6.txt -cs 0
        fi


        # Non exact evaluation using feature level 3
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"NonExactComposedOnlyL3.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -nonEM true -rs $train_test_files"$dataset"/SimComposedHierarchicalJaccardFeaturesL3Alpha0.8.txt -cs 0
        fi

        # Non exact evaluation using only feature 1
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"NonExactComposedOnlyL1.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -nonEM true -rs $train_test_files"$dataset"/SimComposedHierarchicalJaccardFeaturesL1Alpha0.6.txt -cs 0
        fi



        # Only for users defined as Women

        user_feature=female
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $original_datasets/Foursquare_NY/US_NewYorkParsedNoRepFilterUsers2CoreUserProfileGenres.txt -ufs $user_feature -compUF true
        fi

        # Only for users defined as Men
        user_feature=male
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $original_datasets/Foursquare_NY/US_NewYorkParsedNoRepFilterUsers2CoreUserProfileGenres.txt -ufs $user_feature -compUF true
        fi
        # QUARTILES (TRAIN)

        # Only for Q1 Train
        user_feature=Q1
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Train".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/FSNYFilterUsers2CoreRnd8020TrainPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q2 Train
        user_feature=Q2
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Train".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/FSNYFilterUsers2CoreRnd8020TrainPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q3 Train
        user_feature=Q3
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Train".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/FSNYFilterUsers2CoreRnd8020TrainPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q4 Train
        user_feature=Q4
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Train".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/FSNYFilterUsers2CoreRnd8020TrainPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # QUARTILES (TEST)

        # Only for Q1 Test
        user_feature=Q1
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Test".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/FSNYFilterUsers2CoreRnd8020TestPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q2 Test
        user_feature=Q2
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Test".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/FSNYFilterUsers2CoreRnd8020TestPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q3 Test
        user_feature=Q3
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Test".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/FSNYFilterUsers2CoreRnd8020TestPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q4 Test
        user_feature=Q4
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Test".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/FSNYFilterUsers2CoreRnd8020TestPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

      done
      wait
    fi
  done
  wait
done # End dataset
wait
