# !/bin/bash
# Necessary to execute with ./

: '
  Dataset download (Movielens)
  Process data and preparation of splits (Movielens and Foursquare)
  Download MyMedialite framework (for BPR model)
'


original_datasets=OriginalDatasets
java_command=java
jvm_memory=-Xmx30G
JAR=target/AttrEval4RecSys-0.2.1-SNAPSHOT.jar
k_core=2



train_test_files_mov1M=TrainTestMov1M
train_test_files_FSNY=TrainTestFSNYFilterUsers$k_core"Core"


mkdir -p $original_datasets

# For Foursquare
aggregate_strategy=AVERAGE
aggregate_strategy_time=FIRST

# Mvn install for generating the executable file
mvn install

: '
  First, download Movielens1M and prepare the environment
'
if [ ! -d $original_datasets/ml-1m ]; then
  wget http://files.grouplens.org/datasets/movielens/ml-1m.zip
  unzip ml-1m.zip
  mv ml-1m $original_datasets/
  rm ml-1m.zip


  # Now, substitute the separation of the fileds using :: to tabs
  sed 's/::/\t/g' $original_datasets/ml-1m/ratings.dat > $original_datasets/ml-1m/ratingsTab.txt
  sed 's/::/\t/g' $original_datasets/ml-1m/users.dat > $original_datasets/ml-1m/usersTab.txt

else
  echo 'Movielens already exist'
fi



# Converting the file to UTF-8
iconv -f ISO-8859-15 -t UTF-8 $original_datasets/ml-1m/movies.dat > $original_datasets/ml-1m/movies2.dat
$java_command $jvm_memory -jar $JAR -o parseFeaturesMovielens -trf $original_datasets/ml-1m/movies2.dat -orf $original_datasets/ml-1m/FeaturesMovielens.txt -orf2 $original_datasets/ml-1m/MapFeaturesMov.txt

# Obtaining different user features files
$java_command $jvm_memory -jar $JAR -o splitFeatureUserFile -trf $original_datasets/ml-1m/usersTab.txt -ff $original_datasets/ml-1m/movUserGenres.txt,$original_datasets/ml-1m/movUserAge.txt,$original_datasets/ml-1m/movUserProf.txt

# Apply additional filter Movielens for ages (groups from 1-18, 18-35, 35-56 and 56 - ?)
$java_command $jvm_memory -jar $JAR -o groupUserAges -trf $original_datasets/ml-1m/movUserAge.txt -orf $original_datasets/ml-1m/movUserAgeNewIntervals.txt -ufs 1,18,35,56


# Now, begin the splitting (80% train 20% test). Last number is the seed in order to replicate the experiments
if [ ! -d "$train_test_files_mov1M" ]; then
  mkdir -p $train_test_files_mov1M
  $java_command $jvm_memory -jar $JAR -o simpleRandomSplit -trf $original_datasets/ml-1m/ratingsTab.txt $train_test_files_mov1M/Mov1MRnd8020Train.txt $train_test_files_mov1M/Mov1MRnd8020Test.txt 0.8 2242

  $java_command $jvm_memory -jar $JAR -o markUsersByNumberPreferencesQuartiles -trf $train_test_files_mov1M/Mov1MRnd8020Train.txt -orf $train_test_files_mov1M/Mov1MRnd8020TrainPrefsQuartiles.txt

  $java_command $jvm_memory -jar $JAR -o markUsersByNumberPreferencesQuartiles -trf $train_test_files_mov1M/Mov1MRnd8020Test.txt -orf $train_test_files_mov1M/Mov1MRnd8020TestPrefsQuartiles.txt
fi

mv $original_datasets/Mov1/Mov1MFullDirectors.txt $original_datasets/ml-1m/

rmdir $original_datasets/Mov1



# Now, begin the splitting (same as Movielens, 80% for training and 20% for test)
if [ ! -d "$train_test_files_FSNY" ]; then
  mkdir -p $train_test_files_FSNY
  $java_command $jvm_memory -jar $JAR -o simpleRandomSplit -trf $original_datasets/Foursquare_NY/US_NewYorkParsedNoRepFilterUsers"$k_core"Core.txt $train_test_files_FSNY/FSNYFilterUsers"$k_core"CoreRnd8020Train.txt $train_test_files_FSNY/FSNYFilterUsers"$k_core"CoreRnd8020Test.txt 0.8 2242

  $java_command $jvm_memory -jar $JAR -o markUsersByNumberPreferencesQuartiles -trf $train_test_files_FSNY/FSNYFilterUsers"$k_core"CoreRnd8020Train.txt -orf $train_test_files_FSNY/FSNYFilterUsers"$k_core"CoreRnd8020TrainPrefsQuartiles.txt
  $java_command $jvm_memory -jar $JAR -o markUsersByNumberPreferencesQuartiles -trf $train_test_files_FSNY/FSNYFilterUsers"$k_core"CoreRnd8020Test.txt -orf $train_test_files_FSNY/FSNYFilterUsers"$k_core"CoreRnd8020TestPrefsQuartiles.txt

fi




# Obtaining different user features files Foursquare
$java_command $jvm_memory -jar $JAR -o splitFeatureUserFile -trf $original_datasets/Foursquare_NY/US_NewYorkParsedNoRepFilterUsers"$k_core"CoreUserProfile.txt -ff $original_datasets/Foursquare_NY/US_NewYorkParsedNoRepFilterUsers"$k_core"CoreUserProfileGenres.txt,$original_datasets/Foursquare_NY/US_NewYorkParsedNoRepFilterUsers"$k_core"CoreUserProfileTwFriends.txt,$original_datasets/Foursquare_NY/US_NewYorkParsedNoRepFilterUsers"$k_core"CoreUserProfileTwFollow.txt

# Download MyMediaLite for BPR
if [ ! -d "MyMediaLite-3.11" ]; then
  wget "http://mymedialite.net/download/MyMediaLite-3.11.tar.gz"
  tar zxvf MyMediaLite-3.11.tar.gz
  rm MyMediaLite-3.11.tar.gz
fi
