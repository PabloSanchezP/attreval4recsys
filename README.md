# AttrEval4RecSys

Framework to replicate the experiments published in:

[1] Pablo Sánchez, Alejandro Bellogín. Attribute-based evaluation for recommender systems: incorporating user and item attributes in evaluation metrics. In RecSys 2019.


## Getting Started

These instructions will get you a copy of the project up for replicating the experiments of the papers listed above.

### Prerequisites

Java 8

Maven

Mono (for Mymedialite)


## Dependencies
[RankSys 0.4.3](https://github.com/RankSys/RankSys/releases/tag/0.4.3)

Rank fusion library, provided as an independent JAR.

[SeReRSys](https://bitbucket.org/PabloSanchezP/SeReRSys/)

## Instructions

* git clone https://bitbucket.org/PabloSanchezP/AttrEval4RecSys
* cd AttrEval4RecSys

* Steps:
  * ./step1_ParseAndGenerateSplits.sh
  * ./step2_GenerateRecommenders_Foursquare.sh (can be executed in parallel with ./step2_GenerateRecommenders_Mov.sh)
  * ./step2_GenerateRecommenders_Mov.sh (can be executed in parallel with ./step2_GenerateRecommenders_Foursquare.sh)
  * ./step3_PosEval.sh


## Additional algorithms/frameworks used in the experiments
  * [MyMedialite](http://www.mymedialite.net/) (for BPR)


## Authors

* **Pablo Sánchez** - [Universidad Autónoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)
* **Alejandro Bellogín** - [Universidad Autónoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)

## Contact

* **Pablo Sánchez** - <pablo.sanchezp@uam.es>

## License

This project is licensed under [the GNU GPLv3 License](LICENSE.txt)

## Acknowledgments

This work has been funded by the Ministerio de Ciencia, Innovación y Universidades (reference TIN2016-80630-P) and by the European Social Fund (ESF), within the 2017 call for predoctoral contracts.

##Update
  -Version 0.2.1-SNAPSHOT. In this new version this repository do not longer depends on tempcdseqeval and serersys. All the necessary classes from those repositories have been copied to this one.
