#!/bin/sh



#Nomeclature for Datasets
nonaccresultsPrefix=naeval


#For every dataset
datasets="Mov1M"


for dataset in "Mov1M"
do
	summaryFileResultsRanksys=summaryFile"$dataset".dat
	summaryFileResultsRanksysNonExact=summaryFile"$dataset"NonExact.dat


	resultsFolder=resultsFolder"$dataset"
	echo "$resultsFolder"
	rm $summaryFileResultsRanksys
	rm $summaryFileResultsRanksysNonExact


	for evthreshold in 4
	do
		find $resultsFolder/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"* -not -name "*NonExact*"| while read resultFile; do
			resultFileName=$(basename "$resultFile" .txt) #extension removed
			awk -v FILE=$summaryFileResultsRanksys -v TH=$evthreshold -v RNAME=$resultFileName -v DATASET=$dataset 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"DATASET"\t"TH"\t"$0 >> FILE }' $resultFile
		done
		wait

		find $resultsFolder/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"* -name "*NonExact*" | while read resultFile; do
			resultFileName=$(basename "$resultFile" .txt) #extension removed
			awk -v FILE=$summaryFileResultsRanksysNonExact -v TH=$evthreshold -v RNAME=$resultFileName -v DATASET=$dataset 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"DATASET"\t"TH"\t"$0 >> FILE }' $resultFile
		done
		wait




	done #End EvTh
	wait



done #End file
wait


for dataset in "FSNYFilterUsers2Core"
do
	summaryFileResultsRanksys=summaryFile"$dataset".dat
	summaryFileResultsRanksysNonExact=summaryFile"$dataset"NonExact.dat


	resultsFolder=resultsFolder"$dataset"
	echo "$resultsFolder"
	rm $summaryFileResultsRanksys
	rm $summaryFileResultsRanksysNonExact


	for evthreshold in 1
	do
		find $resultsFolder/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"* -not -name "*NonExact*" | while read resultFile; do
			resultFileName=$(basename "$resultFile" .txt) #extension removed
			awk -v FILE=$summaryFileResultsRanksys -v TH=$evthreshold -v RNAME=$resultFileName -v DATASET=$dataset 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"DATASET"\t"TH"\t"$0 >> FILE }' $resultFile
		done
		wait

		find $resultsFolder/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"* -name "*NonExact*" | while read resultFile; do
			resultFileName=$(basename "$resultFile" .txt) #extension removed
			awk -v FILE=$summaryFileResultsRanksysNonExact -v TH=$evthreshold -v RNAME=$resultFileName -v DATASET=$dataset 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"DATASET"\t"TH"\t"$0 >> FILE }' $resultFile
		done
		wait

	done #End EvTh
	wait

done #End file
wait
