/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.main;

import static org.ranksys.formats.parsing.Parsers.lp;
import static org.ranksys.formats.parsing.Parsers.sp;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jooq.lambda.tuple.Tuple3;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.formats.feature.SimpleFeaturesReader;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;
import org.ranksys.formats.rec.RecommendationFormat;
import org.ranksys.formats.rec.SimpleRecommendationFormat;

import es.uam.eps.ir.antimetrics.utils.RecommendationUtils;
import es.uam.eps.ir.attrrec.datamodel.ProcessDataAttributeRecommendation;
import es.uam.eps.ir.attrrec.datamodel.feature.SimpleUserFeatureData;
import es.uam.eps.ir.attrrec.datamodel.feature.SimpleUserFeaturesReader;
import es.uam.eps.ir.attrrec.datamodel.feature.UserFeatureData;
import es.uam.eps.ir.attrrec.metrics.recommendation.averages.WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs;
import es.uam.eps.ir.attrrec.metrics.recommendation.averages.WeightedModelUser;
import es.uam.eps.ir.attrrec.metrics.recommendation.averages.WeightedModelUser.UserMetricWeight;
import es.uam.eps.ir.attrrec.metrics.system.RealAggregateDiversity;
import es.uam.eps.ir.attrrec.metrics.system.UserCoverage;
import es.uam.eps.ir.attrrec.sim.ItemIndexReadingSimilarity;
import es.uam.eps.ir.attrrec.sim.ItemSim;
import es.uam.eps.ir.attrrec.sim.SymmetricSimilarityBean;
import es.uam.eps.ir.attrrec.utils.CBBaselinesUtils.USERTRANSFORMATION;
import es.uam.eps.ir.attrrec.utils.PredicatesStrategies;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.feature.SimpleFeatureData;
import es.uam.eps.ir.ranksys.core.preference.ConcatPreferenceData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;
import es.uam.eps.ir.ranksys.diversity.intentaware.FeatureIntentModel;
import es.uam.eps.ir.ranksys.diversity.intentaware.IntentModel;

import es.uam.eps.ir.ranksys.diversity.sales.metrics.AggregateDiversityMetric;
import es.uam.eps.ir.ranksys.diversity.sales.metrics.GiniIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;
import es.uam.eps.ir.ranksys.metrics.rank.NoDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.BinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.NoRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity;
import es.uam.eps.ir.ranksys.novdiv.distance.CosineFeatureItemDistanceModel;
import es.uam.eps.ir.ranksys.novdiv.distance.ItemDistanceModel;
import es.uam.eps.ir.ranksys.rec.Recommender;




/***
 * Main class to launch the different recommenders / evaluation /data processing
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class Experiment {
	/********************************/
	/** All options of the arguments **/
	/********************************/

	/** Option of the case **/
	private static final String OPT_CASE = "option";

	/** Train and test files **/
	private static final String OPT_TRAIN_FILE = "train file";
	private static final String OPT_TEST_FILE = "test file";

	/** For recommendation (general recommendation) **/
	// Some parameters
	private static final String OPT_ITEMS_RECOMMENDED = "items recommended";
	private static final String OPT_RECOMMENDATION_STRATEGY = "recommendation strategy";
	private static final String OPT_NEIGH = "neighbours";
	private static final String OPT_COMPLETE_INDEXES = "complete indexes";
	private static final String OPT_RANKSYS_SIM = "ranksys similarity";
	private static final String OPT_RANKSYS_REC = "ranksysRecommender";
	private static final String OPT_RANKSYS_REC2 = "ranksysRecommender2";

	private static final String OPT_CONFIDENCE = "confidencesimilarity";
	
	// Parameters for Ranksys Recommender MatrixFactorization
	private static final String OPT_K_FACTORIZER = "k factorizer value";
	private static final String OPT_ALPHA_FACTORIZER = "alpha factorizer value";
	private static final String OPT_LAMBDA_FACTORIZER = "lambda factorizer value";
	private static final String OPT_NUM_INTERACTIONS = "num interactions value";
	
	// Feature to use in cb similarity
	private static final String OPT_NORMALIZE = "normalize";
	

	// Hybrid
	private static final String OPT_HYBRID_WEIGHTS = "HybridWeights";

	/** For generating result files **/
	private static final String OPT_OUT_RESULT_FILE = "output result file";
	private static final String OPT_OUT_RESULT_FILE_2 = "output result file 2";
	private static final String OPT_OVERWRITE = "output result file overwrite";
	private static final String OPT_OUT_SIM_FILE = "output similarity file";

	/** Feature variables (content based support and so on) **/
	private static final String OPT_USER_FEATURE_FILE = "user feature file";
	private static final String OPT_USER_FEATURE_SELECTED = "user feature selected";


	private static final String OPT_FEATURE_READING_FILE = "featureFile";
	private static final String OPT_FEATURE_READING_FILE2 = "featureFile2";
	private static final String OPT_CB_USER_TRANSFORMATION = "cb user transformation";

	/** Variables only used in evaluation **/
	private static final String OPT_RECOMMENDED_FILE = "recommendedFile";
	private static final String OPT_CUTOFF = "ranksysCutoff";
	private static final String OPT_MIN_ITEMS = "minRatingItems";
	private static final String OPT_MIN_USERS = "minRatingUsers";
	private static final String OPT_THRESHOLD = "relevance or matching threshold";
	private static final String OPT_USER_MODEL_WGT = "user weight model weight";

	// Parameters for RankSys non accuracy evaluation
	private static final String OPT_RANKSYS_RELEVANCE_MODEL = "ranksys relevance model";
	private static final String OPT_RANKSYS_DISCOUNT_MODEL = "ranksys discount model";
	private static final String OPT_RANKSYS_BACKGROUND = "ranksys background for relevance model";
	private static final String OPT_RANKSYS_BASE = "ranksys base for discount model";
	

	
	// For evaluation of novelty/diversity/freshness/POI metrics...
	private static final String OPT_ANTI_RELEVANCE_THRESHOLD = "Anti relevance threshold";
	private static final String OPT_COMPUTE_ONLY_ACC = "compute also dividing by the number of users in the test set";
	private static final String OPT_NOT_EXACT_METRIC = "execute the non exact relevance metric";
	private static final String OPT_COMPUTE_USER_FILTER = "compute user filter";



	/** Generate and filter new datasets **/
	// General
	
	// Mappings
	private static final String OPT_MAPPING_ITEMS = "file of mapping items";
	private static final String OPT_MAPPING_USERS = "file of mapping users";
	
	// Generate datasets
	private static final String OPT_NEW_DATASET = "file for the new dataset";
	private static final String OPT_WRAPPER_STRATEGY = "wrapper strategy for ranksys (removing timestamps)";
	private static final String OPT_WRAPPER_STRATEGY_TIMESTAMPS = "wrapper strategy for ranksys (strategy for the timestamps)";
	private static final String OPT_USING_WRAPPER = "using wrapper in ranksys recommenders";
	


	/***********************/
	/** Some default values **/
	/***********************/

	/** For Recommendation **/
	// General
	private static final String DEFAULT_NORMALIZE = "true";
	// Some default values from MFs
	private static final String DEFAULT_FACTORS = "20";
	private static final String DEFAULT_ITERACTIONS = "20";
	// Some default values for HKV
	private static final String DEFAULT_ALPHA_HKV = "1";
	private static final String DEFAULT_LAMBDA_HKV = "0.1";


	// Some default values for recommenders
	private static final String DEFAULT_COMPLETE_INDEXES = "false";
	private static final String DEFAULT_CB_USER_TRANSFORMATION = "WITHRATING";
	// Generate evaluation
	private static final String DEFAULT_ITEMS_RECOMMENDED = "100";
	private static final String DEFAULT_RECOMMENDATION_STRATEGY = "TRAIN_ITEMS";

	private static final String DEFAULT_OVERWRITE = "false";
	private static final String DEFAULT_OPT_COMPUTE_ONLY_ACC = "true";
	private static final String DEFAULT_OPT_NOT_EXACT_METRIC = "false";
	private static final String DEFAULT_COMPUTE_USER_FILTER = "false";

	private static final String DEFAULT_ANTI_RELEVANCE_THRESHOLD = "0";
	private static final String DEFAULT_MATCHING_THRESHOLD = "1";
	private static final String DEFAULT_USER_MODEL_WGT = "NORMAL";

	public static void main(String[] args) throws Exception {
		String step = "";
		CommandLine cl = getCommandLine(args);
		if (cl == null) {
			System.out.println("Error in arguments");
			return;
		}

		// Obtain the arguments these 2 are obligatory
		step = cl.getOptionValue(OPT_CASE);
		String trainFile = cl.getOptionValue(OPT_TRAIN_FILE);

		System.out.println(step);
		switch (step) {

		// Save a IB sim file from a ranksys similarity
		case "ranksysSaveIBSimFile": {
			System.out.println(Arrays.toString(args));
			String outputSimFile = cl.getOptionValue(OPT_OUT_SIM_FILE);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);
			String ransksysSim = cl.getOptionValue(OPT_RANKSYS_SIM);
			String realFeatureFile = cl.getOptionValue(OPT_FEATURE_READING_FILE);
			String confidenceS = cl.getOptionValue(OPT_CONFIDENCE, "0");
			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETE_INDEXES, DEFAULT_COMPLETE_INDEXES));

			String overwrite = cl.getOptionValue(OPT_OVERWRITE, "false");
			double confidence = Double.parseDouble(confidenceS);

			File f = new File(outputSimFile);

			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			FastPreferenceData<Long, Long> ranksysTrainData = AttributeRecommendationUtils.loadTrainFastPreferenceData(trainFile,
					testFile, completeOrNot, true);
			FastPreferenceData<Long, Long> ranksysTestData = AttributeRecommendationUtils.loadTrainFastPreferenceData(trainFile,
					testFile, completeOrNot, false);

			FeatureData<Long, String, Double> featureData = null;
			if (realFeatureFile != null) {
				featureData = SimpleFeatureData.load(SimpleFeaturesReader.get().read(realFeatureFile, lp, sp));
			}

			es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity<Long> simRankSys = AttributeRecommendationUtils
					.obtRanksysItemSimilarity(ranksysTrainData, ransksysSim, featureData);

			Map<SymmetricSimilarityBean<Long>, Tuple3<Double, Integer, Integer>> similarities = new HashMap<SymmetricSimilarityBean<Long>, Tuple3<Double, Integer, Integer>>();

			// For all items
			ranksysTestData.getItemsWithPreferences().filter(i -> ranksysTrainData.containsItem(i)).forEach(i1 -> {
				// Similar to the user
				simRankSys.similarItems(i1).forEach(sim -> {
					similarities.put(new SymmetricSimilarityBean<Long>(i1, sim.v1),
							new Tuple3<Double, Integer, Integer>(sim.v2, -1, -1));
				});
			});

			ItemIndexReadingSimilarity<Long> ibReadingSim = new ItemIndexReadingSimilarity<Long>(similarities,
					ranksysTrainData);
			ibReadingSim.save(outputSimFile, confidence);

		}
			break;

		case "saveComposedSimilarity": {
			System.out.println(Arrays.toString(args));
			String alphas = cl.getOptionValue(OPT_ALPHA_FACTORIZER);
			String features = cl.getOptionValue(OPT_FEATURE_READING_FILE);
			Double confidence = Double.parseDouble(cl.getOptionValue(OPT_CONFIDENCE, "0"));
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);

			FastPreferenceData<Long, Long> ranksysTrainData = AttributeRecommendationUtils.loadTrainFastPreferenceData(trainFile,
					trainFile, false, true);

			List<String> lstFeaturesFiles = new ArrayList<>();
			String[] featuresFiles = features.split(",");
			for (String feat : featuresFiles) {
				lstFeaturesFiles.add(feat);
			}
			List<Double> alphasL = new ArrayList<>();
			String[] alphasS = alphas.split(",");
			for (String alpha : alphasS) {
				alphasL.add(Double.parseDouble(alpha));
			}

			AttributeRecommendationUtils.saveComposedSimilarity(ranksysTrainData, lstFeaturesFiles, alphasL, outputFile,
					confidence);
		}
			break;

		case "generateJaccardSimFileByUserFeatureData": {
			System.out.println(Arrays.toString(args));
			String outputSimFile = cl.getOptionValue(OPT_OUT_SIM_FILE);
			String userFeatureFile = cl.getOptionValue(OPT_USER_FEATURE_FILE);
			FastPreferenceData<Long, Long> rankSysData = AttributeRecommendationUtils
					.loadTrainFastPreferenceData(trainFile, trainFile, false, true);
			UserFeatureData<Long, String, Double> ufD = null;
			if (userFeatureFile != null) {
				ufD = SimpleUserFeatureData.load(SimpleUserFeaturesReader.get().read(userFeatureFile, lp, sp));
			}

			AttributeRecommendationUtils.generateJaccardSimFileByUserFeatureData(rankSysData, ufD, outputSimFile);

		}
			break;

		/***
		 * Pure ranksys recommenders sections (using Preference data with NO timestamps)
		 */
		// RankSys only (not reading similarities). Not working with time nor
		// repetitions datasets
		// (working with FastPreferenceData only)
		case "ranksysOnlyComplete": {
			System.out.println(Arrays.toString(args));

			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String ranksysSimilarity = cl.getOptionValue(OPT_RANKSYS_SIM);

			String ranksysRecommender = cl.getOptionValue(OPT_RANKSYS_REC);
			String additionalRankysRecommenders = cl.getOptionValue(OPT_RANKSYS_REC2);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);

			// MFs Parameters
			Integer numIterations = Integer.parseInt(cl.getOptionValue(OPT_NUM_INTERACTIONS, DEFAULT_ITERACTIONS));
			Integer numFactors = Integer.parseInt(cl.getOptionValue(OPT_K_FACTORIZER, DEFAULT_FACTORS));

			

			// HKV Factorizer
			Double alphaFactorizer = Double.parseDouble(cl.getOptionValue(OPT_ALPHA_FACTORIZER, DEFAULT_ALPHA_HKV));
			Double lambdaFactorizer = Double.parseDouble(cl.getOptionValue(OPT_LAMBDA_FACTORIZER, DEFAULT_LAMBDA_HKV));

			// For candidates items matching a category
			String realFeatureFile = cl.getOptionValue(OPT_FEATURE_READING_FILE2);


			Boolean normalize = Boolean.parseBoolean(cl.getOptionValue(OPT_NORMALIZE, DEFAULT_NORMALIZE));

			final int numberItemsRecommend = Integer.parseInt(cl.getOptionValue(OPT_ITEMS_RECOMMENDED));
			int neighbours = Integer.parseInt(cl.getOptionValue(OPT_NEIGH));

			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATION_STRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);
			String userCBTransformationS = cl.getOptionValue(OPT_CB_USER_TRANSFORMATION,
					DEFAULT_CB_USER_TRANSFORMATION);

			USERTRANSFORMATION ut = AttributeRecommendationUtils.obtCBUserTransformation(userCBTransformationS);

			if (ranksysSimilarity == null) {
				System.out.println("Not working with any recommender using similarities");
			}

			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);

			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && !Boolean.parseBoolean(overwrite)) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETE_INDEXES, DEFAULT_COMPLETE_INDEXES));

			FastPreferenceData<Long, Long> trainPrefData = null;
			FastPreferenceData<Long, Long> testPrefData = null;

			trainPrefData = AttributeRecommendationUtils.loadTrainFastPreferenceData(trainFile, testFile, completeOrNot, true);

			FeatureData<Long, String, Double> featureData = null;
			if (realFeatureFile != null) {
				featureData = SimpleFeatureData.load(SimpleFeaturesReader.get().read(realFeatureFile, lp, sp));
			}

			System.out.println("Not using wrapper (working with no timestamps)");

			Recommender<Long, Long> rankSysrec = AttributeRecommendationUtils
					.obtRankSysRecommeder(ranksysRecommender, additionalRankysRecommenders, ranksysSimilarity,
							trainPrefData, neighbours, numFactors, alphaFactorizer, lambdaFactorizer, numIterations,
							normalize, featureData, ut);

			System.out.println("Analyzing " + testFile);
			System.out.println("Recommender file " + outputFile);

			testPrefData = AttributeRecommendationUtils.loadTrainFastPreferenceData(trainFile, testFile, true, false);

			System.out.println(
					"Writing recommended file. Not items candidates file provided. All candidates are the items not seen by that user in train.");
			PredicatesStrategies.ranksysWriteRanking(trainPrefData, testPrefData, rankSysrec, outputFile,
					numberItemsRecommend, AttributeRecommendationUtils.obtRecommendationStrategy(recommendationStrategy));
		}
			break;

		/***
		 * Skylines recommenders that read the test file and perform the test
		 * recommendations
		 */

		case "skylineRecommenders":
		case "testRecommenders": {
			// They need to receive the test set. We do not work with the train file
			System.out.println(Arrays.toString(args));
			Double antiRelTh = Double
					.parseDouble(cl.getOptionValue(OPT_ANTI_RELEVANCE_THRESHOLD, DEFAULT_ANTI_RELEVANCE_THRESHOLD));
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);
			Double threshold = Double.parseDouble(cl.getOptionValue(OPT_THRESHOLD, DEFAULT_MATCHING_THRESHOLD));
			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETE_INDEXES, DEFAULT_COMPLETE_INDEXES));
			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATION_STRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);
			final int numberItemsRecommend = Integer
					.parseInt(cl.getOptionValue(OPT_ITEMS_RECOMMENDED, DEFAULT_ITEMS_RECOMMENDED));

			String rankSysRecommender = cl.getOptionValue(OPT_RANKSYS_REC);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);

			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			FastPreferenceData<Long, Long> ranksysTrainDataOriginal = AttributeRecommendationUtils
					.loadTrainFastPreferenceData(trainFile, testFile, completeOrNot, true);

			FastPreferenceData<Long, Long> ranksysTestDataOriginal = AttributeRecommendationUtils
					.loadTrainFastPreferenceData(trainFile, testFile, true, false);



			Recommender<Long, Long> rec = AttributeRecommendationUtils.obtRankSysSkylineRecommender(rankSysRecommender,
					ranksysTestDataOriginal, threshold, antiRelTh);

			PredicatesStrategies.ranksysWriteRanking(ranksysTrainDataOriginal, ranksysTestDataOriginal, rec, outputFile,
					numberItemsRecommend, AttributeRecommendationUtils.obtRecommendationStrategy(recommendationStrategy));

		}
			break;

		/***
		 * Data preprocessing section.
		 */



		// Simple random split
		case "simpleRandomSplit": {
			System.out.println(Arrays.toString(args));

			long seed = System.currentTimeMillis();
			if (args.length >= 8) {
				System.out.println("Using custom seed");
				seed = Long.parseLong(args[7]);
			}

			ProcessDataAttributeRecommendation.randomSplit(trainFile, args[4], args[5], Double.parseDouble(args[6]),
					seed);
		}
			break;

		case "splitFeatureUserFile": {
			System.out.println(Arrays.toString(args));
			String userDifferentFeatures = cl.getOptionValue(OPT_FEATURE_READING_FILE);
			// The train file is the user feature file
			ProcessDataAttributeRecommendation.splitFeatureUserFile(trainFile, userDifferentFeatures.split(","));

		}
			break;

		case "markUsersByNumberPreferencesQuartiles": {
			System.out.println(Arrays.toString(args));
			String outputResult = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			final PreferenceData<Long, Long> trainData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp));
			ProcessDataAttributeRecommendation.markUsersByNumberPreferencesQuartiles(trainData, outputResult);
		}
			break;

		case "groupUserAges": {
			System.out.println(Arrays.toString(args));
			String outputUserFeatureFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String featureAgesUserInterval = cl.getOptionValue(OPT_USER_FEATURE_SELECTED);
			String edadesS[] = featureAgesUserInterval.split(",");

			TreeSet<Integer> edades = new TreeSet<>();
			for (String edad : edadesS) {
				int edadParsed = Integer.parseInt(edad);
				edades.add(edadParsed);
			}

			ProcessDataAttributeRecommendation.groupUserAges(trainFile, outputUserFeatureFile, edades);
		}
			break;


		case "ParseMyMediaLite": {
			System.out.println("-o ParseMyMediaLite -trf myMediaLiteRecommendation testFile newRecommendation");
			System.out.println(Arrays.toString(args));
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = AttributeRecommendationUtils
					.retrieveTrainTestIndexes(args[4], args[4], false, lp, lp);

			List<Long> usersTest = indexes.v1;
			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());

			RecommendationUtils.parseMyMediaLite(trainFile, userIndexTest, args[5]);
		}
			break;

		case "parseFeaturesMovielens": {
			System.out.println(Arrays.toString(args));
			String outputResultFeatureFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String outputResultFeatureFile2 = cl.getOptionValue(OPT_OUT_RESULT_FILE_2);
			ProcessDataAttributeRecommendation.parseFeaturesMovielens(trainFile, outputResultFeatureFile,
					outputResultFeatureFile2);

		}
			break;

		/**
		 * RankSys evaluation section
		 */
		// Ranksys with non accuracy metrics
		case "ranksysNonAccuracyWithoutFeatureMetricsEvaluation":
		case "ranksysNonAccuracyMetricsEvaluation": {

			/*
			 * -Train file -Test file -Recommended file -Item feature file -Ranksys Metric
			 * -Output file -Threshold -Cutoff
			 */
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);
			String recommendedFile = cl.getOptionValue(OPT_RECOMMENDED_FILE);
			String itemFeatureFile = cl.getOptionValue(OPT_FEATURE_READING_FILE);
			String userFeaturFile = cl.getOptionValue(OPT_USER_FEATURE_FILE);
			String userSelFeature = cl.getOptionValue(OPT_USER_FEATURE_SELECTED);

			int threshold = Integer.parseInt(cl.getOptionValue(OPT_THRESHOLD));
			String cutoffs = cl.getOptionValue(OPT_CUTOFF);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);
			String userWeightModel = cl.getOptionValue(OPT_USER_MODEL_WGT, DEFAULT_USER_MODEL_WGT);

			Boolean computeOnlyAcc = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPUTE_ONLY_ACC, DEFAULT_OPT_COMPUTE_ONLY_ACC));

			Boolean computeNonExactMetric = Boolean
					.parseBoolean(cl.getOptionValue(OPT_NOT_EXACT_METRIC, DEFAULT_OPT_NOT_EXACT_METRIC));
			Boolean computeUserFilter = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPUTE_USER_FILTER, DEFAULT_COMPUTE_USER_FILTER));

			String ranksysRelevanceModel = cl.getOptionValue(OPT_RANKSYS_RELEVANCE_MODEL);
			String ranksysDiscountModel = cl.getOptionValue(OPT_RANKSYS_DISCOUNT_MODEL);
			String ranksysBackground = cl.getOptionValue(OPT_RANKSYS_BACKGROUND);
			String ranksysBase = cl.getOptionValue(OPT_RANKSYS_BASE);

			String itemSim = cl.getOptionValue(OPT_RANKSYS_SIM);

			File f = new File(outputFile);
			// If file of ranksys evaluation already exist then nothing
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			final FastPreferenceData<Long, Long> trainDataFast = AttributeRecommendationUtils.loadTrainFastPreferenceData(trainFile,
					testFile, false, true);
			final PreferenceData<Long, Long> trainData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp));
			final PreferenceData<Long, Long> testData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp));

			final PreferenceData<Long, Long> totalData = new ConcatPreferenceData<>(trainData, testData);

			final Set<Long> testUsers = testData.getUsersWithPreferences().collect(Collectors.toSet());

			final PreferenceData<Long, Long> originalRecommendedData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(recommendedFile, lp, lp));
			// recommended data has to be filtered to avoid evaluating users not in test
			final PreferenceData<Long, Long> recommendedData = AttributeRecommendationUtils
					.filterPreferenceData(originalRecommendedData, testUsers, null);

			////////////////////////
			// INDIVIDUAL METRICS //
			////////////////////////
			FeatureData<Long, String, Double> featureData = null;
			ItemDistanceModel<Long> dist = null;
			IntentModel<Long, Long, String> intentModel = null;
			if (!computeOnlyAcc) {
				featureData = SimpleFeatureData.load(SimpleFeaturesReader.get().read(itemFeatureFile, lp, sp));
				// COSINE DISTANCE
				dist = new CosineFeatureItemDistanceModel<>(featureData);
				// INTENT MODEL
				intentModel = new FeatureIntentModel<>(totalData, featureData);
			}

			// Binary relevance and anti relevance model for ranking metrics
			BinaryRelevanceModel<Long, Long> binRel = new BinaryRelevanceModel<>(false, testData, threshold);

			// Relevance model for novelty/diversity (can be with or without relevance)
			RelevanceModel<Long, Long> selectedRelevance = null;

			double ranksysBackgroundD = 0.0;
			double ranksysBaseD = 0.0;

			if (ranksysBackground != null) {
				ranksysBackgroundD = Double.parseDouble(ranksysBackground);
			}

			if (ranksysBase != null) {
				ranksysBaseD = Double.parseDouble(ranksysBase);
			}

			if (ranksysRelevanceModel == null) {
				selectedRelevance = new NoRelevanceModel<>();
			} else {
				selectedRelevance = AttributeRecommendationUtils.obtRelevanceModelRanksys(ranksysRelevanceModel,
						testData, threshold, ranksysBackgroundD);
			}

			RankingDiscountModel discModel = null;
			if (ranksysDiscountModel == null) {
				discModel = new NoDiscountModel();
			} else {
				discModel = AttributeRecommendationUtils.obtRankingDiscountModel(ranksysDiscountModel, ranksysBaseD);
			}

			int numUsersTest = testData.numUsersWithPreferences();
			int numUsersRecommended = recommendedData.numUsersWithPreferences();
			int numItems = totalData.numItemsWithPreferences(); // Num items with preferences in the data
			System.out.println("\n\nNum users in the test set " + numUsersTest);
			System.out.println("\n\nNum users to whom we have made recommendations " + numUsersRecommended);
			System.out.println("Modified ratios normalization");

			Map<String, SystemMetric<Long, Long>> sysMetrics = new HashMap<>();
			// Ranking metrics (avg for recommendation)
			Map<String, RecommendationMetric<Long, Long>> recMetricsAvgRelUsers = new HashMap<>();
			Map<String, RecommendationMetric<Long, Long>> recMetricsAllRecUsers = new HashMap<>();

			String[] differentCutoffs = cutoffs.split(",");

			
			ItemIndexReadingSimilarity<Long> isim = null;
			ItemSimilarity<Long> simItemSim = null;
			
			if (itemSim != null) {
				isim = new ItemIndexReadingSimilarity<>(itemSim, Double.NaN, lp,
						trainDataFast);
				simItemSim = new ItemSim<>(trainDataFast, isim);
			}
			
			for (String cutoffS : differentCutoffs) {
				int cutoff = Integer.parseInt(cutoffS);
				AttributeRecommendationUtils.addMetrics(recMetricsAvgRelUsers, recMetricsAllRecUsers, threshold, cutoff,
						trainData, trainDataFast, testData, selectedRelevance, binRel, discModel, featureData, dist,
						intentModel, step, computeOnlyAcc, computeNonExactMetric, simItemSim);

				sysMetrics.put("aggrdiv@" + cutoff, new AggregateDiversityMetric<>(cutoff, selectedRelevance));
				sysMetrics.put("gini@" + cutoff, new GiniIndex<>(cutoff, numItems));
				sysMetrics.put("RealAD@" + cutoff, new RealAggregateDiversity<Long, Long>(cutoff));
				sysMetrics.put("usercov", new UserCoverage<Long, Long>());

			}

			UserFeatureData<Long, String, Double> ufD = null;
			if (userFeaturFile != null) {
				ufD = SimpleUserFeatureData.load(SimpleUserFeaturesReader.get().read(userFeaturFile, lp, sp));
			}
			UserFeatureData<Long, String, Double> ufD2 = ufD;

			UserMetricWeight weight = AttributeRecommendationUtils.obtUserMetricWeight(userWeightModel);
			System.out.println("Working with user weight " + weight.toString());

			WeightedModelUser<Long, Long, String, Double> wmu = new WeightedModelUser<>(trainData, weight,
					userSelFeature, ufD);

			recMetricsAvgRelUsers.forEach((name, metric) -> sysMetrics.put(name + "_rec",
					new WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<>(metric, binRel, wmu)));

			recMetricsAllRecUsers.forEach((name, metric) -> sysMetrics.put(name + "_rec",
					new WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<>(metric, binRel, wmu)));

			RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);

			System.out.println("Computing user filter: " + computeUserFilter);

			format.getReader(recommendedFile).readAll().filter(rec -> AttributeRecommendationUtils
					.isValidUser(rec.getUser(), computeUserFilter, ufD2, userSelFeature))
					.forEach(rec -> sysMetrics.values().forEach(metric -> metric.add(rec)));

			PrintStream out = new PrintStream(new File(outputFile));
			sysMetrics.forEach((name, metric) -> out.println(name + "\t" + metric.evaluate()));
			out.close();
			
			

		}
			break;

		default:
			System.out.println("Option " + step + " not recognized");
			break;
		}
	}
	


	/**
	 * Method that will obtain a command line using the available options of the
	 * arguments
	 *
	 * @param args
	 *            the arguments that the program will receive
	 * @return a command line if the arguments are correct or null if an error
	 *         occurred
	 */
	private static CommandLine getCommandLine(String[] args) {
		Options options = new Options();

		// Number of the case
		Option caseIdentifier = new Option("o", OPT_CASE, true, "option of the case");
		caseIdentifier.setRequired(true);
		options.addOption(caseIdentifier);

		// Train file
		Option trainFile = new Option("trf", OPT_TRAIN_FILE, true, "input file train path");
		trainFile.setRequired(true);
		options.addOption(trainFile);

		// Here not required
		// TestFile file
		Option testFile = new Option("tsf", OPT_TEST_FILE, true, "input file test path");
		testFile.setRequired(false);
		options.addOption(testFile);

		// Neighbours
		Option neighbours = new Option("n", OPT_NEIGH, true, "neighbours");
		neighbours.setRequired(false);
		options.addOption(neighbours);

		// threshold
		Option threshold = new Option("thr", OPT_THRESHOLD, true, "relevance or matching threshold");
		threshold.setRequired(false);
		options.addOption(threshold);

		// NumberItemsRecommended
		Option numberItemsRecommended = new Option("nI", OPT_ITEMS_RECOMMENDED, true, "Number of items recommended");
		numberItemsRecommended.setRequired(false);
		options.addOption(numberItemsRecommended);

		// numberMinRatItems
		Option numberMinRatItems = new Option("mri", OPT_MIN_ITEMS, true, "Minimum number of item ratings");
		numberMinRatItems.setRequired(false);
		options.addOption(numberMinRatItems);

		// numberMinRatUsers
		Option numberMinRatUsers = new Option("mru", OPT_MIN_USERS, true, "Minimum number of user ratings");
		numberMinRatUsers.setRequired(false);
		options.addOption(numberMinRatUsers);

		// OutResultfile
		Option outfile = new Option("orf", OPT_OUT_RESULT_FILE, true, "output result file");
		outfile.setRequired(false);
		options.addOption(outfile);

		// OutResultfile 2
		Option outfile2 = new Option("orf2", OPT_OUT_RESULT_FILE_2, true, "output result file (2)");
		outfile2.setRequired(false);
		options.addOption(outfile2);

		// OutSimilarityfile
		Option outSimfile = new Option("osf", OPT_OUT_SIM_FILE, true, "output similarity file");
		outSimfile.setRequired(false);
		options.addOption(outSimfile);

		// Ranksys similarity
		Option rankSysSim = new Option("rs", OPT_RANKSYS_SIM, true, "ranksys similarity");
		rankSysSim.setRequired(false);
		options.addOption(rankSysSim);

		// Ranksys recommender
		Option rankSysRecommender = new Option("rr", OPT_RANKSYS_REC, true, "ranksys recommeder");
		rankSysRecommender.setRequired(false);
		options.addOption(rankSysRecommender);

		// Ranksys recommender 2
		Option rankSysRecommender2 = new Option("rr2", OPT_RANKSYS_REC2, true, "additional ranksys recommeder");
		rankSysRecommender2.setRequired(false);
		options.addOption(rankSysRecommender2);

		// Overwrite result
		Option outputOverwrite = new Option("ovw", OPT_OVERWRITE, true, "overwrite");
		outputOverwrite.setRequired(false);
		options.addOption(outputOverwrite);

		// Feature reading file
		Option featureReadingFile = new Option("ff", OPT_FEATURE_READING_FILE, true, "features file");
		featureReadingFile.setRequired(false);
		options.addOption(featureReadingFile);

		// Feature reading file2
		Option featureReadingFile2 = new Option("ff2", OPT_FEATURE_READING_FILE2, true, "features file (2)");
		featureReadingFile2.setRequired(false);
		options.addOption(featureReadingFile2);

		// Feature reading file
		Option recommendedFile = new Option("rf", OPT_RECOMMENDED_FILE, true, "recommended file");
		recommendedFile.setRequired(false);
		options.addOption(recommendedFile);

		// RankSysMetric
		Option ranksysCutoff = new Option("rc", OPT_CUTOFF, true, "ranksyscutoff");
		ranksysCutoff.setRequired(false);
		options.addOption(ranksysCutoff);

		// Confidence similarity (if similarity is lower than this value, the similarity
		// will consider )
		Option confidenceSimilarity = new Option("cs", OPT_CONFIDENCE, true, "confidence similarity");
		confidenceSimilarity.setRequired(false);
		options.addOption(confidenceSimilarity);

		// CB User transformation
		Option cbUserTransformation = new Option("userTransformationCB", OPT_CB_USER_TRANSFORMATION, true,
				"cb user transformation");
		cbUserTransformation.setRequired(false);
		options.addOption(cbUserTransformation);

		// RankSys factorizers (k)
		Option kFactorizer = new Option("kFactorizer", OPT_K_FACTORIZER, true, "k factorizer");
		kFactorizer.setRequired(false);
		options.addOption(kFactorizer);

		// RankSys factorizers (alpha)
		Option alhpaFactorizer = new Option("aFactorizer", OPT_ALPHA_FACTORIZER, true, "alpha factorizer");
		alhpaFactorizer.setRequired(false);
		options.addOption(alhpaFactorizer);

		// RankSys factorizers (lambda)
		Option lambdaFactorizer = new Option("lFactorizer", OPT_LAMBDA_FACTORIZER, true, "lambda factorizer");
		lambdaFactorizer.setRequired(false);
		options.addOption(lambdaFactorizer);

		// RankSys factorizers (numInteractions)
		Option numInteractionsFact = new Option("nIFactorizer", OPT_NUM_INTERACTIONS, true,
				"numInteractions factorizer");
		numInteractionsFact.setRequired(false);
		options.addOption(numInteractionsFact);

		// RansksyLibrary NonAccuracyEvaluationParameters
		Option ranksysRelevanceModel = new Option("ranksysRelModel", OPT_RANKSYS_RELEVANCE_MODEL, true,
				"ranksys relevance model");
		ranksysRelevanceModel.setRequired(false);
		options.addOption(ranksysRelevanceModel);

		Option ranksysDiscountModel = new Option("ranksysDiscModel", OPT_RANKSYS_DISCOUNT_MODEL, true,
				"ranksys discount model");
		ranksysDiscountModel.setRequired(false);
		options.addOption(ranksysDiscountModel);

		Option ranksysBackground = new Option("ranksysBackRel", OPT_RANKSYS_BACKGROUND, true,
				"ranksys background for relevance model");
		ranksysBackground.setRequired(false);
		options.addOption(ranksysBackground);

		Option ranksysBase = new Option("ranksysBaseDisc", OPT_RANKSYS_BASE, true, "ranksys base for discount model");
		ranksysBase.setRequired(false);
		options.addOption(ranksysBase);

		// File containing 2 columns, old id and new id, for datasets that we have
		// changed the ids (for items)
		Option itemsMapping = new Option("IMapping", OPT_MAPPING_ITEMS, true, "mapping file of items");
		itemsMapping.setRequired(false);
		options.addOption(itemsMapping);

		// File containing 2 columns, old id and new id, for datasets that we have
		// changed the ids
		Option usersMapping = new Option("UMapping", OPT_MAPPING_USERS, true, "mapping file of users");
		usersMapping.setRequired(false);
		options.addOption(usersMapping);

		// Option for creating a new dataset when transforming the users and items
		Option newDatasetFile = new Option("newDataset", OPT_NEW_DATASET, true, "new dataset destination");
		newDatasetFile.setRequired(false);
		options.addOption(newDatasetFile);

		// Option of the wrapper strategy to parse datasets or to use it in ranksys
		Option wrapperStrategyPreferences = new Option("wStrat", OPT_WRAPPER_STRATEGY, true,
				"strategy to use in the wrapper");
		wrapperStrategyPreferences.setRequired(false);
		options.addOption(wrapperStrategyPreferences);

		// Option of the wrapper strategy to parse datasets or to use it in ranksys
		Option wrapperStrategyTimeStamps = new Option("wStratTime", OPT_WRAPPER_STRATEGY_TIMESTAMPS, true,
				"strategy to use in the wrapper (for timestamps)");
		wrapperStrategyTimeStamps.setRequired(false);
		options.addOption(wrapperStrategyTimeStamps);

		Option usingWrapper = new Option("usingWrapper", OPT_USING_WRAPPER, true, "using wrapper in ranksys");
		usingWrapper.setRequired(false);
		options.addOption(usingWrapper);

		Option useCompleteIndex = new Option("cIndex", OPT_COMPLETE_INDEXES, true,
				"use the complete indexes (train + test)");
		useCompleteIndex.setRequired(false);
		options.addOption(useCompleteIndex);

		Option recommendationStrategy = new Option("recStrat", OPT_RECOMMENDATION_STRATEGY, true,
				"recommendation strategy");
		recommendationStrategy.setRequired(false);
		options.addOption(recommendationStrategy);

		Option antiRelevanceThreshold = new Option("antiRelTh", OPT_ANTI_RELEVANCE_THRESHOLD, true,
				"integer in order to indicate the antiRelevanceTh");
		antiRelevanceThreshold.setRequired(false);
		options.addOption(antiRelevanceThreshold);

		Option computeOnlyAccuracy = new Option("onlyAcc", OPT_COMPUTE_ONLY_ACC, true,
				"if we are computing just accuracy metrics or also novelty-diversity");
		computeOnlyAccuracy.setRequired(false);
		options.addOption(computeOnlyAccuracy);

		Option normalize = new Option("normalize", OPT_NORMALIZE, true, "apply a normalization function");
		normalize.setRequired(false);
		options.addOption(normalize);

		Option userModelWeight = new Option("userModelW", OPT_USER_MODEL_WGT, true, "user model weight");
		userModelWeight.setRequired(false);
		options.addOption(userModelWeight);

		Option userFeatureFile = new Option("uff", OPT_USER_FEATURE_FILE, true, "user feature file");
		userFeatureFile.setRequired(false);
		options.addOption(userFeatureFile);

		Option userFeatureSelected = new Option("ufs", OPT_USER_FEATURE_SELECTED, true, "user feature selected");
		userFeatureSelected.setRequired(false);
		options.addOption(userFeatureSelected);

		Option notExactMetric = new Option("nonEM", OPT_NOT_EXACT_METRIC, true, "not exact metric");
		notExactMetric.setRequired(false);
		options.addOption(notExactMetric);

		Option computeUserFilter = new Option("compUF", OPT_COMPUTE_USER_FILTER, true, "compute user filter");
		computeUserFilter.setRequired(false);
		options.addOption(computeUserFilter);

		Option hybridWeights = new Option("hybridWeights", OPT_HYBRID_WEIGHTS, true, "hybrid weights");
		hybridWeights.setRequired(false);
		options.addOption(hybridWeights);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			return null;
		}
		return cmd;

	}

}
