/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.main;

import static org.ranksys.formats.parsing.Parsers.lp;
import static org.ranksys.formats.parsing.Parsers.sp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.formats.feature.SimpleFeaturesReader;
import org.ranksys.formats.parsing.Parser;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;

import es.uam.eps.ir.antimetrics.recommenders.SkylineAntiRelevanceRecommender;
import es.uam.eps.ir.antimetrics.recommenders.SkylineRelevanceRecommender;
import es.uam.eps.ir.attrrec.datamodel.feature.UserFeatureData;
import es.uam.eps.ir.attrrec.metrics.recommendation.averages.WeightedModelUser.UserMetricWeight;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.NonExactNDCG;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.NonExactPrecision;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model.NonExactNDCGRelevanceModel;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model.NonExactRelevanceModel;
import es.uam.eps.ir.attrrec.sim.ItemIndexReadingSimilarity;
import es.uam.eps.ir.attrrec.sim.SymmetricSimilarityBean;
import es.uam.eps.ir.attrrec.sim.UserIndexReadingSimilarity;
import es.uam.eps.ir.attrrec.sim.cb.BaselineCB;
import es.uam.eps.ir.attrrec.sim.cb.CBCosineItemSimilarity;
import es.uam.eps.ir.attrrec.sim.cb.CBCosineUserSimilarity;
import es.uam.eps.ir.attrrec.sim.cb.composed.CBHierarchicalJaccardComposedItemSimilarity;
import es.uam.eps.ir.attrrec.utils.CBBaselinesUtils;
import es.uam.eps.ir.attrrec.utils.CBBaselinesUtils.USERTRANSFORMATION;
import es.uam.eps.ir.attrrec.utils.PredicatesStrategies;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.feature.SimpleFeatureData;
import es.uam.eps.ir.ranksys.core.preference.ConcatPreferenceData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;
import es.uam.eps.ir.ranksys.diversity.distance.metrics.EILD;
import es.uam.eps.ir.ranksys.diversity.intentaware.IntentModel;
import es.uam.eps.ir.ranksys.diversity.intentaware.metrics.AlphaNDCG;
import es.uam.eps.ir.ranksys.diversity.intentaware.metrics.ERRIA;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.basic.AveragePrecision;
import es.uam.eps.ir.ranksys.metrics.basic.NDCG;
import es.uam.eps.ir.ranksys.metrics.basic.ReciprocalRank;
import es.uam.eps.ir.ranksys.metrics.rank.ExponentialDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.LogarithmicDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.NoDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.ReciprocalDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.BackgroundBinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.BinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.NoRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.mf.Factorization;
import es.uam.eps.ir.ranksys.mf.als.HKVFactorizer;
import es.uam.eps.ir.ranksys.mf.rec.MFRecommender;
import es.uam.eps.ir.ranksys.nn.item.ItemNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.CachedItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.ItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.TopKItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.SetCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.SetJaccardItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorJaccardItemSimilarity;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.sim.SetCosineUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.SetJaccardUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorCosineUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorJaccardUserSimilarity;
import es.uam.eps.ir.ranksys.novdiv.distance.ItemDistanceModel;
import es.uam.eps.ir.ranksys.novelty.longtail.FDItemNovelty;
import es.uam.eps.ir.ranksys.novelty.longtail.PCItemNovelty;
import es.uam.eps.ir.ranksys.novelty.longtail.metrics.EFD;
import es.uam.eps.ir.ranksys.novelty.longtail.metrics.EPC;
import es.uam.eps.ir.ranksys.novelty.unexp.PDItemNovelty;
import es.uam.eps.ir.ranksys.novelty.unexp.metrics.EPD;
import es.uam.eps.ir.ranksys.rec.Recommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.PopularityRecommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.RandomRecommender;



/***
 * Class containing useful methods for obtaining recommenders, processing results and obtaning similarities
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class AttributeRecommendationUtils {

	
	/**
	 * Method to obtain the recommendation strategy
	 * 
	 * @param stringRS
	 *            the recomendation strategy read
	 * @return the enumeration value or null
	 */
	public static PredicatesStrategies.recommendationStrategy obtRecommendationStrategy(String stringRS) {
		for (PredicatesStrategies.recommendationStrategy ut : PredicatesStrategies.recommendationStrategy.values()) {
			if (ut.toString().equals(stringRS) || ut.getShortName().equals(stringRS)) {
				System.out.println("Selected " + ut.toString());
				return ut;
			}
		}
		return null;
	}
	

	
	/**
	 * Method to obtain the content based strategy
	 * 
	 * @param stringUT
	 *            the content based user transformation read
	 * @return
	 */
	public static CBBaselinesUtils.USERTRANSFORMATION obtCBUserTransformation(String stringUT) {
		for (CBBaselinesUtils.USERTRANSFORMATION ut : CBBaselinesUtils.USERTRANSFORMATION.values()) {
			if (ut.toString().equals(stringUT)) {
				return ut;
			}
		}
		return null;
	}
	
	
	/***
	 * Method to obtain a tuple of users and items from a file
	 * 
	 * @param file
	 *            the file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	public static <U, I> Tuple2<List<U>, List<I>> getUserItemsFromFile(String file, Parser<U> up, Parser<I> ip) {
		try {
			PreferenceData<U, I> data = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(file, up, ip));
			List<U> usersList = data.getAllUsers().collect(Collectors.toList());
			List<I> itemsList = data.getAllItems().collect(Collectors.toList());

			System.out.println("Ordering by longs");

			return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
					itemsList.stream().sorted().collect(Collectors.toList()));
		} catch (IOException e) {
			System.out.println("Exception catched");
			e.printStackTrace();
			return null;
		}

	}
	
	/***
	 * Method to obtain the user and the items from the users and items files
	 * 
	 * @param fileTrain
	 *            the train file
	 * @param fileTest
	 *            the test file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	public static <U, I> Tuple2<List<U>, List<I>> getCompleteUserItems(String fileTrain, String fileTest, Parser<U> up,
			Parser<I> ip) {
		try {
			PreferenceData<U, I> trainData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(fileTrain, up, ip));
			PreferenceData<U, I> testData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(fileTest, up, ip));
			PreferenceData<U, I> totalData = new ConcatPreferenceData<>(trainData, testData);
			List<U> usersList = totalData.getAllUsers().collect(Collectors.toList());
			List<I> itemsList = totalData.getAllItems().collect(Collectors.toList());

			return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
					itemsList.stream().sorted().collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	/***
	 * Method to retrieve the train test indexes from a file
	 * 
	 * @param completeIndexes
	 *            flag indicating if we are going to concatenate the indexes of
	 *            train and test or not
	 * @param trainFile
	 *            the train file
	 * @param testFile
	 *            the test file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	public static <U, I> Tuple4<List<U>, List<I>, List<U>, List<I>> retrieveTrainTestIndexes(String trainFile,
			String testFile, boolean completeIndexes, Parser<U> up, Parser<I> ip) {
		List<U> usersTrain = null;
		List<I> itemsTrain = null;
		List<U> usersTest = null;
		List<I> itemsTest = null;
		if (completeIndexes) {
			Tuple2<List<U>, List<I>> userItems = getCompleteUserItems(trainFile, testFile, up, ip);
			usersTrain = userItems.v1;
			itemsTrain = userItems.v2;
			usersTest = userItems.v1;
			itemsTest = userItems.v2;
		} else {
			Tuple2<List<U>, List<I>> userItemsTrain = getUserItemsFromFile(trainFile, up, ip);
			usersTrain = userItemsTrain.v1;
			itemsTrain = userItemsTrain.v2;
			Tuple2<List<U>, List<I>> userItemsTest = getUserItemsFromFile(testFile, up, ip);
			usersTest = userItemsTest.v1;
			itemsTest = userItemsTest.v2;

		}
		return new Tuple4<>(usersTrain, itemsTrain, usersTest, itemsTest);
	}
	
	/***
	 * Method to create a SimpleFastTemporalFeaturePreferenceData
	 * 
	 * @param trainFile
	 *            the train file
	 * @param testFile
	 *            the test file
	 * @param completeOrNot
	 *            if we will also use the test indexes to build the data
	 * @return
	 */
	public static FastPreferenceData<Long, Long> loadTrainFastPreferenceData(String trainFile, String testFile,
			boolean completeOrNot, boolean useTrainTest) {
		try {
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = retrieveTrainTestIndexes(trainFile, testFile, completeOrNot, lp, lp);
			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;
			List<Long> usersTest = indexes.v3;
			List<Long> itemsTest = indexes.v4;

			// Retrieving train
			if (useTrainTest) {
				FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
				FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());

				return SimpleFastPreferenceData.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp),
						userIndexTrain, itemIndexTrain);
			} else {
				// Retrieving test
				FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
				FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());

				return SimpleFastPreferenceData.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp),
						userIndexTest, itemIndexTest);
			}
		} catch (IOException e) {
			System.out.println("Error while creating SimpleFastTemporalFeaturePreferenceData");
			e.printStackTrace();
		}
		return null;
	}
	
	/**
     * Method to obtain the relevance model from RankSys
     * @param model String of the relevance model
     * @param prefData the preference data
     * @param threshold the threshold
     * @param background only required for background
     * @return the relevance model
     */
    public static <U, I> RelevanceModel<U, I> obtRelevanceModelRanksys(String model, PreferenceData<U, I> prefData, int threshold, double background) {
        switch (model) {
            case "BinaryRelevanceModel":
                return new BinaryRelevanceModel<>(false, prefData, threshold);
            case "NoRelevanceModel":
                return new NoRelevanceModel<U, I>();
            case "BackgroundBinaryRelevanceModel":
                return new BackgroundBinaryRelevanceModel<>(false, prefData, threshold, background);
            default:
                return null;
        }
    }
	
	 /**
     * Method to obtain the ranking discount model
     * @param model the model in a string
     * @param base only required for the exponential discount model
     * @return the RankSys Discount model
     */
    public static <U, I> RankingDiscountModel obtRankingDiscountModel(String model, double base) {
        switch (model) {
            case "ExponentialDiscountModel":
                return new ExponentialDiscountModel(base);
            case "LogarithmicDiscountModel":
                return new LogarithmicDiscountModel();
            case "NoDiscountModel":
                return new NoDiscountModel();
            case "ReciprocalDiscountModel":
                return new ReciprocalDiscountModel();
            default:
                return null;
        }
    }
	
	public static Recommender<Long, Long> obtRankSysRecommeder(String rec, String additionalRecs, String similarity,
			FastPreferenceData<Long, Long> data, int kNeighbours, int kFactorizer, double alphaF, double lambdaF,
			int numInteractions, boolean normalize, FeatureData<Long, String, Double> featureData, 
			USERTRANSFORMATION ut) {

		switch (rec) {
		case "RndRec":
		case "RandomRecommender": {
			System.out.println("RandomRecommender");
			return new RandomRecommender<>(data, data);
		}

		case "PopRec":
		case "PopularityRecommender": {
			System.out.println("PopularityRecommender");
			return new PopularityRecommender<>(data);
		}
		case "UBKnnRec":
		case "UserNeighborhoodRecommender": { // User based. Pure CF recommendation
			es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simUNR = obtRanksysUserSimilarity(data, similarity);
			if (simUNR == null) {
				return null;
			} else {
				System.out.println("UserNeighborhoodRecommender");
				System.out.println("kNeighs: " + kNeighbours);
				System.out.println("Sim: " + similarity);
			}
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simUNR, kNeighbours);
			return new UserNeighborhoodRecommender<>(data, urneighborhood, 1);
		}

		case "IBKnnRec":
		case "ItemNeighborhoodRecommender": {// Item based. Pure CF recommendation
			es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity<Long> simINR = obtRanksysItemSimilarity(data, similarity);
			if (simINR == null) {
				return null;
			} else {
				System.out.println("ItemNeighborhoodRecommender");
				System.out.println("kNeighs: " + kNeighbours);
				System.out.println("Sim: " + similarity);
			}
			ItemNeighborhood<Long> neighborhood = new TopKItemNeighborhood<>(simINR, kNeighbours);
			neighborhood = new CachedItemNeighborhood<>(neighborhood);
			return new ItemNeighborhoodRecommender<>(data, neighborhood, 1);
		}

		case "MFRecHKV":
		case "MFRecommenderHKV": { // Matrix factorization
			int k = kFactorizer;
			double lambda = lambdaF;
			double alpha = alphaF;
			int numIter = numInteractions;
			System.out.println("MFRecommenderHKV");
			System.out.println("kFactors: " + k);
			System.out.println("lambda: " + lambda);
			System.out.println("alpha: " + alpha);
			System.out.println("numIter: " + numIter);

			DoubleUnaryOperator confidence = x -> 1 + alpha * x;
			Factorization<Long, Long> factorization = new HKVFactorizer<Long, Long>(lambda, confidence, numIter)
					.factorize(k, data);
			return new MFRecommender<>(data, data, factorization);
		}
		
		case "UBCBRec":
		case "UBContentBasedRecommender": {
			System.out.println("kNeighs: " + kNeighbours);
			Map<Long, Map<String, Double>> mapUsers = CBBaselinesUtils.getUsersCBTransformation(data, featureData, ut,
					normalize);
			Map<Integer, Map<String, Double>> mapUidx = CBBaselinesUtils.getUidxCBTransFormation(data, mapUsers);
			CBCosineUserSimilarity<Long, String> ucbCosSim = new CBCosineUserSimilarity<Long, String>(data, mapUidx);
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(ucbCosSim, kNeighbours);
			return new UserNeighborhoodRecommender<>(data, urneighborhood, 1);

		}
		case "BaseCB":
		case "BaselineCB": {
			return new BaselineCB<>(data, featureData);
		}

		default:
			System.out.println("Carefull. Recommender is null");
			return null;
		}

	}

	public static <F, V> void generateJaccardSimFileByUserFeatureData(FastUserIndex<Long> userIndex,
			UserFeatureData<Long, F, V> ufD, String outputFile) {
		Map<SymmetricSimilarityBean<Long>, Tuple3<Double, Integer, Integer>> similarities = new HashMap<SymmetricSimilarityBean<Long>, Tuple3<Double, Integer, Integer>>();

		userIndex.getAllUsers().forEach(u1 -> {
			Set<F> userFeatures1 = ufD.getUserFeatures(u1).map(t -> t.v1).collect(Collectors.toSet());
			userIndex.getAllUsers().filter(u2 -> !u1.equals(u2)).forEach(u2 -> {
				Set<F> userFeatures2 = ufD.getUserFeatures(u2).map(t -> t.v1).collect(Collectors.toSet());
				Set<F> union = new HashSet<>(userFeatures1);
				union.addAll(userFeatures2);
				Set<F> intersection = new HashSet<>(userFeatures1);
				intersection.retainAll(userFeatures2);
				if (intersection.size() != 0 && union.size() != 0) {
					double value = (double) intersection.size() / union.size();

					similarities.put(new SymmetricSimilarityBean<Long>(u1, u2),
							new Tuple3<Double, Integer, Integer>(value, -1, -1));
				}
			});
		});
		UserIndexReadingSimilarity<Long> userReadingSim = new UserIndexReadingSimilarity<Long>(similarities, userIndex);
		userReadingSim.save(outputFile, 0.0);

	}

	/**
	 * Obtain RankSys Item Similarity
	 *
	 * @param data
	 *            the preference data used to compute the similarities
	 * @param similarity
	 *            the string identifier of the similarity
	 * @return a RankSys item similarity model
	 */
	public static <U, I, F> es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity<I> obtRanksysItemSimilarity(
			FastPreferenceData<U, I> data, String similarity, FeatureData<I, F, Double> featureData) {
		switch (similarity) {
		case "VectorCosineItemSimilarity":
			// 0.5 to make it symmetrical.
			return new VectorCosineItemSimilarity<>(data, 0.5, true);
		case "VectorJaccardItemSimilarity":
			return new VectorJaccardItemSimilarity<>(data, true);
		case "SetJaccardItemSimilarity":
			return new SetJaccardItemSimilarity<>(data, true);
		case "SetCosineItemSimilarity":
			return new SetCosineItemSimilarity<>(data, 0.5, true);
		case "CBCosineItemSimilarity":
			Map<I, Map<F, Double>> mapResult = CBBaselinesUtils.binaryItemTranformation(data, featureData);
			Map<Integer, Map<F, Double>> mapIindex = CBBaselinesUtils.getIidxCBTransFormation(data, mapResult);
			return new CBCosineItemSimilarity<>(data, mapIindex);
		default:
			System.out.println(similarity + " not recognized");
			return null;
		}
	}

	/****
	 * Method to save a composed similarity
	 * 
	 * @param fstPreferenceData
	 * @param pathsFeaturesData
	 * @param alphas
	 * @param outputFile
	 * @param confidence
	 * @throws IOException
	 */
	public static void saveComposedSimilarity(FastPreferenceData<Long, Long> fstPreferenceData,
			List<String> pathsFeaturesData, List<Double> alphas, String outputFile, Double confidence)
			throws IOException {
		List<Map<Integer, Map<String, Double>>> idxsFeatures = new ArrayList<>();
		for (String pathFeatureData : pathsFeaturesData) {
			FeatureData<Long, String, Double> featureData = SimpleFeatureData
					.load(SimpleFeaturesReader.get().read(pathFeatureData, lp, sp));
			Map<Long, Map<String, Double>> mapResult = CBBaselinesUtils.binaryItemTranformation(fstPreferenceData,
					featureData);
			Map<Integer, Map<String, Double>> mapIindex = CBBaselinesUtils.getIidxCBTransFormation(fstPreferenceData,
					mapResult);
			idxsFeatures.add(mapIindex);
		}

		ItemSimilarity<Long> simRankSys = new CBHierarchicalJaccardComposedItemSimilarity<Long, String>(
				fstPreferenceData, idxsFeatures, alphas);

		Map<SymmetricSimilarityBean<Long>, Tuple3<Double, Integer, Integer>> similarities = new HashMap<SymmetricSimilarityBean<Long>, Tuple3<Double, Integer, Integer>>();

		// For all items
		fstPreferenceData.getItemsWithPreferences().forEach(i1 -> {
			// Similar to the item
			simRankSys.similarItems(i1).forEach(sim -> {
				similarities.put(new SymmetricSimilarityBean<Long>(i1, sim.v1),
						new Tuple3<Double, Integer, Integer>(sim.v2, -1, -1));
			});
		});

		ItemIndexReadingSimilarity<Long> ibReadingSim = new ItemIndexReadingSimilarity<Long>(similarities,
				fstPreferenceData);
		ibReadingSim.save(outputFile, confidence);
	}

	public static <U, F> boolean isValidUser(U user, Boolean computeFilter, UserFeatureData<U, F, Double> userFeature,
			F feature) {
		if (userFeature == null || feature == null || !computeFilter) {
			return true;
		}

		return userFeature.getUserFeatures(user).map(tuples -> tuples.v1).anyMatch(f -> f.equals(feature));
	}

	/***
	 * Method to get the user model weight
	 * 
	 * @param userModelWeight
	 * @return the user model weight
	 */
	public static UserMetricWeight obtUserMetricWeight(String userModelWeight) {
		for (UserMetricWeight m : UserMetricWeight.values()) {
			if (m.toString().equals(userModelWeight) || m.getShortName().equals(userModelWeight)) {
				return m;
			}
		}
		return null;
	}

	/***
	 * RankSys skyline recommenders
	 * 
	 * @param rec
	 * @param testData
	 * @param dataTemporalTest
	 * @param userIndex
	 * @param itemIndex
	 * @param relevanceThreshold
	 * @param antiRelevanceThreshold
	 * @return
	 */
	public static Recommender<Long, Long> obtRankSysSkylineRecommender(String rec, FastPreferenceData<Long, Long> testData,
			double relevanceThreshold, double antiRelevanceThreshold) {
		switch (rec) {
		case "skyRelRec":
		case "SkyRelRec":
		case "skylineRelevanceRecommender":
		case "SkylineRelevanceRecommender": {
			System.out.println("SkylineRelevanceRecommender");
			return new SkylineRelevanceRecommender<>(testData, relevanceThreshold);
		}
		case "skyAntiRelRec":
		case "SkyAntiRelRec":
		case "skylineAntiRelevanceRecommender":
		case "SkylineAntiRelevanceRecommender": {
			System.out.println("SkylineAntiRelevanceRecommender");
			return new SkylineAntiRelevanceRecommender<>(testData, antiRelevanceThreshold);
		}
		default:
			System.out.println("Recommender not recognized. Recommender is null");
			return null;
		}
	}

	public static TreeMap<Double, Double> defaultSimsForNonExactTreeMap() {
		TreeMap<Double, Double> result = new TreeMap<>();
		result.put(0.0, 0.0);
		result.put(0.5, 0.25);
		result.put(0.75, 0.5);
		result.put(1.0, 0.75);
		return result;

	}

	public static void addMetrics(Map<String, RecommendationMetric<Long, Long>> recMetricsAvgRelUsers,
			Map<String, RecommendationMetric<Long, Long>> recMetricsAllRecUsers, int threshold, int cutoff,
			PreferenceData<Long, Long> trainData, FastPreferenceData<Long, Long> trainDataFast,
			PreferenceData<Long, Long> testData, RelevanceModel<Long, Long> selectedRelevance,
			BinaryRelevanceModel<Long, Long> binRel, RankingDiscountModel discModel,
			FeatureData<Long, String, Double> featureData, ItemDistanceModel<Long> dist,
			IntentModel<Long, Long, String> intentModel, String step, Boolean computeOnlyAcc,
			Boolean computeNonExactMetric, ItemSimilarity<Long> simItemSim) {

		recMetricsAvgRelUsers.put("Precision@" + cutoff + "_" + threshold,
				new es.uam.eps.ir.ranksys.metrics.basic.Precision<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("MAP@" + cutoff + "_" + threshold, new AveragePrecision<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("Recall@" + cutoff + "_" + threshold,
				new es.uam.eps.ir.ranksys.metrics.basic.Recall<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("MRR@" + cutoff + "_" + threshold, new ReciprocalRank<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("NDCG@" + cutoff + "_" + threshold,
				new NDCG<>(cutoff, new NDCG.NDCGRelevanceModel<>(false, testData, threshold)));

		if (simItemSim != null) {

			recMetricsAvgRelUsers.put("NonExactPrecision@" + cutoff + "_" + threshold,
					new NonExactPrecision<>(cutoff, new NonExactRelevanceModel<>(false, testData, threshold, simItemSim,
							0, defaultSimsForNonExactTreeMap())));
			recMetricsAvgRelUsers.put("NonExactNDCG@" + cutoff + "_" + threshold,
					new NonExactNDCG<>(cutoff, new NonExactNDCGRelevanceModel<>(false, testData, threshold,
							simItemSim, 0, defaultSimsForNonExactTreeMap())));
		}

		if (!computeOnlyAcc) {
			recMetricsAllRecUsers.put("epc@" + cutoff,
					new EPC<>(cutoff, new PCItemNovelty<>(trainData), selectedRelevance, discModel));
			recMetricsAllRecUsers.put("efd@" + cutoff,
					new EFD<>(cutoff, new FDItemNovelty<>(trainData), selectedRelevance, discModel));
		}

		// If we have features
		if (!computeOnlyAcc && featureData != null) {
			recMetricsAllRecUsers.put("eild@" + cutoff, new EILD<>(cutoff, dist, selectedRelevance, discModel));
			recMetricsAllRecUsers.put("epd@" + cutoff,
					new EPD<>(cutoff, new PDItemNovelty<>(false, trainData, dist), selectedRelevance, discModel));
			recMetricsAllRecUsers.put("err-ia@" + cutoff,
					new ERRIA<>(cutoff, intentModel, new ERRIA.ERRRelevanceModel<>(false, testData, threshold)));
			recMetricsAllRecUsers.put("a-ndcg@" + cutoff, new AlphaNDCG<>(cutoff, 0.5, featureData, binRel));
		}

	}
	
	/***
	 * Method to filter the preference data by providing the set of valid users and
	 * valid items
	 * 
	 * @param original
	 *            the original preferences
	 * @param validUsers
	 *            the set of valid users
	 * @param validItems
	 *            the set of valid items
	 * @return the preference data filtered
	 */
	public static <U, I> PreferenceData<U, I> filterPreferenceData(PreferenceData<U, I> original, Set<U> validUsers,
			Set<I> validItems) {
		final List<Tuple3<U, I, Double>> tuples = new ArrayList<>();
		original.getUsersWithPreferences().filter(u -> validUsers.contains(u)).forEach(u -> {
			if (validItems != null) {
				original.getUserPreferences(u).filter(t -> (validItems.contains(t.v1))).forEach(idPref -> {
					tuples.add(new Tuple3<>(u, idPref.v1, idPref.v2));
				});
			} else {
				original.getUserPreferences(u).forEach(idPref -> {
					tuples.add(new Tuple3<>(u, idPref.v1, idPref.v2));
				});
			}
		});
		System.out.println("Tuples original: " + original.numPreferences());
		System.out.println("Tuples original filtered: " + tuples.size());
		Stream<Tuple3<U, I, Double>> prev = tuples.stream();
		PreferenceData<U, I> result = SimplePreferenceData.load(prev);
		return result;
	}
	
	/**
	 * Obtain RankSys User Similarity
	 *
	 * @param data
	 *            the preference data used to compute the similarities
	 * @param similarity
	 *            the similarity
	 * @return a RankSys user similarity model
	 */
	public static <U, I> es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<U> obtRanksysUserSimilarity(
			FastPreferenceData<U, I> data, String similarity) {
		switch (similarity) {
		case "VectorCosineUserSimilarity":
			// 0.5 to make it symmetrical.
			return new VectorCosineUserSimilarity<>(data, 0.5, true);
		case "VectorJaccardUserSimilarity":
			return new VectorJaccardUserSimilarity<>(data, true);
		case "SetJaccardUserSimilarity":
			return new SetJaccardUserSimilarity<>(data, true);
		case "SetCosineUserSimilarity":
			return new SetCosineUserSimilarity<>(data, 0.5, true);
		case "PearsonUserCorrelation":

		default:
			System.out.println("RankSys user similarity is null");
			return null;
		}
	}
	
	/**
	 * Obtain RankSys Item Similarity
	 *
	 * @param data
	 *            the preference data used to compute the similarities
	 * @param similarity
	 *            the string identifier of the similarity
	 * @return a RankSys item similarity model
	 */
	public static es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity<Long> obtRanksysItemSimilarity(
			FastPreferenceData<Long, Long> data, String similarity) {
		switch (similarity) {
		case "VectorCosineItemSimilarity":
			// 0.5 to make it symmetrical.
			return new VectorCosineItemSimilarity<>(data, 0.5, true);
		case "VectorJaccardItemSimilarity":
			return new VectorJaccardItemSimilarity<>(data, true);
		case "SetJaccardItemSimilarity":
			return new SetJaccardItemSimilarity<>(data, true);
		case "SetCosineItemSimilarity":
			return new SetCosineItemSimilarity<>(data, 0.5, true);

		default:
			return null;
		}
	}

}
