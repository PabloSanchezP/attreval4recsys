/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.sim.cb.composed;

import java.util.List;
import java.util.Map;

import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity;

/***
 * Content Based Hierarchical jaccard composed similarity
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <I>
 * @param <F>
 */
public class CBHierarchicalJaccardComposedItemSimilarity<I, F> extends ItemSimilarity<I> {

	public CBHierarchicalJaccardComposedItemSimilarity(FastItemIndex<I> iIndex,
			List<Map<Integer, Map<F, Double>>> idxsFeatures, List<Double> alphas) {
		super(iIndex, new CBHierarchicalJaccardComposedSimilarity<>(idxsFeatures, alphas));
	}

}
