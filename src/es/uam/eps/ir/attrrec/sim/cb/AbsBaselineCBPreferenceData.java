/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.sim.cb;

import es.uam.eps.ir.attrrec.utils.CBBaselinesUtils;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/***
 * Abstract class extending baseline CB for preference data without timestamps
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 */
public abstract class AbsBaselineCBPreferenceData<U, I, F> extends AbsBaselineCB<U, I, F> {
	protected FastPreferenceData<U, I> dataModel;

	public AbsBaselineCBPreferenceData(FastPreferenceData<U, I> dataModel, FeatureData<I, F, Double> featureData,
			boolean normalize) {
		super(dataModel, dataModel, featureData, normalize);
		this.dataModel = dataModel;
	}

	public void trainSimple() {

		// If its TF-IDF we need the featuresItems map
		if (it.equals(CBBaselinesUtils.ITEMTRANSFORMATION.TF_IDF)) {
			featuresItems = CBBaselinesUtils.computeFeaturesItemsSimple(dataModel, featureData, normalize);
		}

		itemFeatures = CBBaselinesUtils.itemTransformation(dataModel, featureData, featuresItems, it, normalize);

		userFeatures = CBBaselinesUtils.getUsersCBTransformation(dataModel, featureData, ut, normalize);
	}

	@Override
	public Int2DoubleMap getScoresMap(int uidx) {
		Int2DoubleOpenHashMap scoresMap = new Int2DoubleOpenHashMap();
		scoresMap.defaultReturnValue(0.0);

		if (uidx == -1) {
			return scoresMap;
		}
		dataModel.getAllIidx().forEach(iidx -> {
			scoresMap.put(iidx, computeScore(this.uidx2user(uidx), this.iidx2item(iidx)));
		});

		return scoresMap;

	}

}
