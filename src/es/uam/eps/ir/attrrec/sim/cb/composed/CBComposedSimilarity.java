/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.sim.cb.composed;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.IntToDoubleFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.ranksys.nn.sim.Similarity;

/**
 * Similarity that will be formed by a list of different sets of features The
 * method to compute the similarity between 2 indexes will be abstract
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <F>
 */
public abstract class CBComposedSimilarity<F> implements Similarity {

	protected List<Map<Integer, Map<F, Double>>> idxsFeatures;
	protected List<Double> alphas;
	private Set<Integer> allIndexes;

	public CBComposedSimilarity(List<Map<Integer, Map<F, Double>>> idxsFeatures, List<Double> alphas) {
		this.idxsFeatures = idxsFeatures;
		this.alphas = alphas;
		this.allIndexes = new TreeSet<>();
		for (int i = 0; i < idxsFeatures.size(); i++) {
			this.allIndexes.addAll(idxsFeatures.get(i).keySet());
		}
	}

	@Override
	public IntToDoubleFunction similarity(int idx) {
		return idx2 -> {
			return sim(idx, idx2);
		};

	}

	@Override
	public Stream<Tuple2id> similarElems(int idx) {
		return IntStream.range(0, allIndexes.size()).filter(idx2 -> idx2 != idx).boxed().map(idx2 -> {
			return new Tuple2id(idx2, sim(idx, idx2));
		}).filter(t -> t.v2 != 0);
	}

	protected abstract double sim(int idx1, int idx2);

}
