/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.sim.cb.composed;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/***
 * Composed similarity using the jaccard coefficient
 * 
 * At the moment we find a similarity different from 0, we finish
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <F>
 */
public class CBHierarchicalJaccardComposedSimilarity<F> extends CBComposedSimilarity<F> {

	public CBHierarchicalJaccardComposedSimilarity(List<Map<Integer, Map<F, Double>>> idxsFeatures,
			List<Double> alphas) {
		super(idxsFeatures, alphas);
	}

	@Override
	protected double sim(int idx1, int idx2) {
		for (int i = 0; i < idxsFeatures.size(); i++) {
			Map<F, Double> featuresidx1 = idxsFeatures.get(i).get(idx1);
			Map<F, Double> featuresidx2 = idxsFeatures.get(i).get(idx2);
			double jac = jaccardMaps(featuresidx1, featuresidx2);
			if (jac > 0.0) {
				return jac * this.alphas.get(i);
			}
		}

		return 0.0;
	}

	private double jaccardMaps(Map<F, Double> featuresidx1, Map<F, Double> featuresidx2) {
		double accNum = 0;
		Set<F> unionFeatures = new HashSet<>();
		if (featuresidx1 == null || featuresidx2 == null || featuresidx1.isEmpty() || featuresidx2.isEmpty()) {
			return 0;
		}
		unionFeatures.addAll(featuresidx1.keySet());
		unionFeatures.addAll(featuresidx2.keySet());

		for (F feature1 : featuresidx1.keySet()) {
			if (featuresidx2.get(feature1) != null) {
				accNum += 1.0;
			}
		}

		return accNum / unionFeatures.size();
	}

}
