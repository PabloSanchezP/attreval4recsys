/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.sim.cb.composed;

import java.util.List;
import java.util.Map;

import es.uam.eps.ir.attrrec.utils.CBBaselinesUtils;


/***
 * Composed similarity using the cosine between the feutures of the items
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <F>
 */
public class CBCosineAggregateComposedSimilarity<F> extends CBComposedSimilarity<F> {

	public CBCosineAggregateComposedSimilarity(List<Map<Integer, Map<F, Double>>> idxsFeatures, List<Double> alphas) {
		super(idxsFeatures, alphas);
	}

	@Override
	protected double sim(int idx1, int idx2) {
		double acc = 0;
		for (int i = 0; i < idxsFeatures.size(); i++) {
			acc += CBBaselinesUtils.cosineCBFeatureSimilarity(idxsFeatures.get(i).get(idx1),
					idxsFeatures.get(i).get(idx2)) * alphas.get(i);
		}
		return acc;
	}
}
