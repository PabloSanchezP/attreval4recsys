/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.sim.cb;

import java.util.Map;
import java.util.function.IntToDoubleFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.attrrec.utils.CBBaselinesUtils;
import es.uam.eps.ir.ranksys.nn.sim.Similarity;

/***
 * Content based Cosine similarity
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <F>
 */
public class CBCosineSimilarity<F> implements Similarity {

	Map<Integer, Map<F, Double>> idxFeatures;

	public CBCosineSimilarity(Map<Integer, Map<F, Double>> idxFeatures) {
		this.idxFeatures = idxFeatures;
	}

	@Override
	public IntToDoubleFunction similarity(int idx) {
		return idx2 -> {
			return CBBaselinesUtils.cosineCBFeatureSimilarity(idxFeatures.get(idx), idxFeatures.get(idx2));
		};
	}

	@Override
	public Stream<Tuple2id> similarElems(int idx) {
		int totalRows = idxFeatures.keySet().size();
		return IntStream.range(0, totalRows).filter(idx2 -> idx2 != idx).boxed().map(idx2 -> {
			return new Tuple2id(idx2,
					CBBaselinesUtils.cosineCBFeatureSimilarity(idxFeatures.get(idx), idxFeatures.get(idx2)));
		});
	}

}
