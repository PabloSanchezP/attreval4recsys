/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.sim.cb;

import es.uam.eps.ir.attrrec.utils.CBBaselinesUtils;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;

/***
 * Pure content based Baseline. In this recommender it stores for each item and
 * each user a map with the value of the features. In items, this value of
 * features are binary while in the users is the accumulation of he values
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class BaselineCB<U, I, F> extends AbsBaselineCBPreferenceData<U, I, F> {

	/***
	 * Constructor of baseline content based recommender
	 * 
	 * @param dataModel
	 *            the dataModel
	 * @param feature
	 *            the feature to make transformation
	 */
	public BaselineCB(FastPreferenceData<U, I> dataModel, FeatureData<I, F, Double> fD) {
		super(dataModel, fD, false);
		trainSimple();
	}

	@Override
	public double computeScore(U user, I item) {
		return CBBaselinesUtils.cosineCBFeatureSimilarity(this.userFeatures.get(user), this.itemFeatures.get(item));
	}

}
