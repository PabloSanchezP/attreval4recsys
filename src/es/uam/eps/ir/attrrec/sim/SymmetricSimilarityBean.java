/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.sim;

import java.util.Objects;

/**
 * Class that stores the similarity between 2 users
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <T>
 *            the type of the element
 */
public class SymmetricSimilarityBean<T extends Comparable<T>> {

	/***
	 * Two similarities are the same if: -User1 and user2 are the same to the other
	 * class user1 and user 2 -If user1 = other class user2 and user2 == other class
	 * user1
	 */

	T element1;

	T element2;

	public SymmetricSimilarityBean(T userA, T userB) {
		if (userA.compareTo(userB) < 0) { // Done like this to see that similarities are symmetrical
			this.element1 = userA;
			this.element2 = userB;
		} else {
			this.element1 = userB;
			this.element2 = userA;
		}
	}

	public T getElement1() {
		return element1;
	}

	public T getElement2() {
		return element2;
	}

	@Override
	public String toString() {
		return "LCSSymmetricSimilarityBean [element1=" + element1 + ", element2=" + element2 + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(element1, element2);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		SymmetricSimilarityBean<T> other = (SymmetricSimilarityBean<T>) obj;
		if (other.element1.equals(this.element1) && other.element2.equals(this.element2))
			return true;
		return false;
	}
}
