/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.utils;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.rec.Recommender;

/****
 * File that will contains all the predicates that may work as recommendation strategies of the system.
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class PredicatesStrategies {
	
	/**
	 * Enumeration of the predicates:
	 * -NO_CONDITION: no conditions of the items recommended (can be rated by the user in train)
	 * 
	 * -TRAIN_ITEMS: every item recommended by the user must have a rating in train data and must have not been rated in the train subset by the user
	 * 
	 * -ALL_ITEMS: every item in the system is considered or the recommendation, but must not have been rated by the user in train 
	 * 
	 * -ONLYITEMS_TEST: only make recommendations for the items that are in the user test
	 * 
	 * -ITEMS_IN_TRAIN: the items rated must be in the train set
	 * 
	 * @author Pablo Sanchez (pablo.sanchezp@estudiante.uam.es)
	 *
	 */
	public enum recommendationStrategy {
		NO_CONDITION("N_C"), TRAIN_ITEMS("T_I"), ALL_ITEMS("A_I"), ONLYITEMS_TEST("OI_T"), ITEMS_IN_TRAIN("I_I_T");
		
		private String shortName;
		
		private recommendationStrategy(String name) {
			this.shortName = name;
		}
		
		public String getShortName() {
			return shortName;
		}
	};
	
	/**
     * Predicate used in RankSys to make recommendations of items not rated by the user in train
     *
     * @param user the user
     * @param trainData the ranksysDatamodel of train
     * @return a predicate
     */
    public static <U, I> Predicate<I> isNotInTrain(U user, PreferenceData<U, I> trainData) {
        return p -> trainData.getUserPreferences(user).noneMatch(itemPref -> itemPref.v1.equals(p));
    }
    
    /**
     * Predicate so see if an item is a candidate
     * @param candidates
     * @return a predicate
     */
    public static <I> Predicate<I> isCandidate(Set<I> candidates){
    	return p -> candidates.contains(p);
    }
    
    /**
     * Predicate used in RankSys to make recommendations of items that appears in the test set
     *
     * @param user the user
     * @param testData the ranksysDatamodel of test
     * @return a predicate
     */
    public static <U, I> Predicate<I> isInTest(U user, PreferenceData<U, I> testData) {
        return p -> testData.getUserPreferences(user).anyMatch(itemPref -> itemPref.v1.equals(p));
    }
    
    /***
     * Predicate that will return true if the item has been rated in the training set (by any user)
     * @param trainData the trainData
     * @return a predicate
     */
    public static <U, I> Predicate<I> itemWithRatingInTrain(PreferenceData<U, I> trainData) {
    	return p-> trainData.getItemPreferences(p).findFirst().isPresent();
    }
    
    /***
     * Train items strategy of the predicate. 
     * -Only items with at least one rating in train should be recommended.
     * -Only items that the user have not rated in train (new items for the user).
     * @param user the user
     * @param trainData the trainData
     * @return a predicate
     */
    public static <U,I> Predicate<I> trainItems(U user, PreferenceData<U, I> trainData){
    	return isNotInTrain(user, trainData).and(itemWithRatingInTrain(trainData));
    }
    
    /***
     * No condition predicate (every item is considered as a predicate)
     * @return a predicate
     */
    public static <I> Predicate<I> noCondition(){
    	return p -> true;
    }
    
    /**
     * *
     * Method that receiving an user, the train data and a stream of
     * candidateItems, will return a stream of items that are not in the train
     * of the user but that have at least 1 preference in the train subset 
     * and is in the list of candidate items
     *
     * @param user the user to whom we are making recommendations
     * @param trainData the train data
     * @param candidateItem the candidate items
     * @return a stream of candidates items
     */
    public static <U, I> Stream<I> generateCandidatesWithRatingsAndNotRatedByUserInTrain(U user, PreferenceData<U, I> trainData, Set<I> candidateItems) {
        return trainData.getItemsWithPreferences().filter(i -> !trainData.getUserPreferences(user).map(pref -> pref.v1).
                anyMatch(i2 -> i2.equals(i))).filter(i -> candidateItems.contains(i)); 

    }
    
    /**
     * *
     * Method to store a ranking file of a RankSys recommender
     *
     * @param ranksysTrainData the train data
     * @param ranksystestData the test data 
     * @param rankSysrec the recommender
     * @param outputFile the output file
     * @param numberItemsRecommend the number of items to recommend
     */
    public static <U, I> void ranksysWriteRanking(PreferenceData<U, I> ranksysTrainData, PreferenceData<U, I> ranksystestData, Recommender<U, I> rankSysrec, String outputFile, int numberItemsRecommend, recommendationStrategy strat) {
        PrintStream out;
        try {
            out = new PrintStream(outputFile);
            Stream<U> targetUsers = ranksystestData.getUsersWithPreferences();
            System.out.println("Users in test data " + ranksystestData.numUsersWithPreferences());
            System.out.println("Users in test data appearing in train set "+ ranksystestData.getUsersWithPreferences().filter(u -> ranksysTrainData.containsUser(u)).count());
            if (strat.equals(recommendationStrategy.ONLYITEMS_TEST))
            	numberItemsRecommend = Integer.MAX_VALUE;
            final int numItemsRec = numberItemsRecommend;
            targetUsers.forEach(user -> {
                if (ranksystestData.getUserPreferences(user).count() != 0L && ranksysTrainData.containsUser(user)) {
                    int rank = 1;
                    Recommendation<U, I> rec = rankSysrec.getRecommendation(user, numItemsRec,
                    		selectRecommendationPredicate(strat,user,ranksysTrainData, ranksystestData));
                    for (Tuple2od<I> tup : rec.getItems()) { // Items recommended
                        // We should see if the items are in train
                        out.println(formatRank(user, tup.v1, tup.v2, rank));
                        rank++;
                    }
                }
            });
            out.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    
    /**
     * Method to create a recommender file in which every item recommended must be in the set of candidates
     * 
     * @param ranksysTrainData the train data
     * @param ranksystestData the test data
     * @param rankSysrec the recommended
     * @param outputFile the output file
     * @param numberItemsRecommend the number of items recommended
     * @param candidates the set of candidates
     * @param strat the strategy to apply 
     */
    public static <U, I> void ranksysWriteRankingFromCandidatesList(FastPreferenceData<U, I> ranksysTrainData, FastPreferenceData<U, I> ranksystestData, Recommender<U, I> rankSysrec, String outputFile, int numberItemsRecommend, Set<I> candidates, recommendationStrategy strat) {
        PrintStream out;
        try {
            out = new PrintStream(outputFile);
            Stream<U> targetUsers = ranksystestData.getUsersWithPreferences();
            System.out.println("Users in test data " + ranksystestData.numUsersWithPreferences());
            targetUsers.forEach(user -> {
                if (ranksystestData.getUserPreferences(user).count() != 0L && ranksysTrainData.containsUser(user)) {
                    int rank = 1;
                    Recommendation<U, I> rec = rankSysrec.getRecommendation(user, selectRecommendationPredicate(strat,user,ranksysTrainData, ranksystestData, candidates));
                    if (rec == null || rec.getItems() == null) {
                        System.out.println("Recommendation for user " + user + " is null");
                    } else {
                        for (Tuple2od<I> tup : rec.getItems()) { // Items recommended
                            if (rank > numberItemsRecommend) {
                                break;
                            }
                            out.println(formatRank(user, tup.v1, tup.v2, rank));
                            rank++;
                        }
                    }
                }
            });
            out.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    
    /***
     * Selection of the recommendation predicate
     * @param strat the strategy of the recommendation
     * @param user the user
     * @param trainData the train data
     * @param testData the test data
     * @return a predicate for recommendation
     */
    private static <U,I> Predicate<I> selectRecommendationPredicate(recommendationStrategy strat, U user, PreferenceData<U, I> trainData, PreferenceData<U, I> testData){
    	switch (strat){
    		case ALL_ITEMS:
    			return isNotInTrain(user, trainData);
    		case NO_CONDITION:
    			return noCondition();
    		case ONLYITEMS_TEST:
    			return isInTest(user, testData);
    		case ITEMS_IN_TRAIN:
    			return itemWithRatingInTrain(trainData);
    		case TRAIN_ITEMS:
    		default:
    			return trainItems(user,trainData);
    	}
    	
    }
    
    /**
     * Selection of recommendation predicate combining the set of candidate files 
     * @param strat the strategy of the recommendation
     * @param user the user
     * @param trainData the trainData
     * @param testData the testData
     * @return the predicate
     */
    private static <U,I> Predicate<I> selectRecommendationPredicate(recommendationStrategy strat, U user, PreferenceData<U, I> trainData, PreferenceData<U, I> testData, Set<I> candidates){
    	switch (strat){
    		case ALL_ITEMS:
    			return isNotInTrain(user, trainData).and(isCandidate(candidates));
    		case NO_CONDITION:
    			return isCandidate(candidates);
    		case ONLYITEMS_TEST:
    			return isInTest(user, testData).and(isCandidate(candidates));
    		case ITEMS_IN_TRAIN:
    			return itemWithRatingInTrain(trainData);
    		case TRAIN_ITEMS:
    		default:
    			return trainItems(user,trainData).and(isCandidate(candidates));
    	}
    	
    }
    
    /***
     * Method to print the recommendations for all the recommenders implemented in the framework
     * @param user the user identifier
     * @param item the item identifier
     * @param valueItem the score of the recommendation
     * @param rank the rank
     * @return the rank
     */
    public static String formatRank(Object user, Object item, double valueItem, int rank) {
        return user.toString() + "\t" + item.toString() + "\t" + valueItem + "\t" + rank;
    }
    
}
