/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.metrics.system;

import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;

/**
 * System metric of real aggregate diversity (ranksys version is not the pure
 * aggregate diversity) in recommenders system handbook
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class RealAggregateDiversity<U, I> implements SystemMetric<U, I> {

	private Set<I> items = new TreeSet<>();
	private int cutOff;

	public RealAggregateDiversity(int cutOff) {
		items = new TreeSet<>();
		this.cutOff = cutOff;
	}

	@Override
	public void add(Recommendation<U, I> recommendation) {
		int min = Math.min(cutOff, recommendation.getItems().size());
		Set<I> newItems = recommendation.getItems().subList(0, min).stream().map(Tuple2od::v1)
				.collect(Collectors.toSet());
		items.addAll(newItems);
	}

	@Override
	public double evaluate() {
		return items.size();
	}

	@Override
	public void combine(SystemMetric<U, I> other) {
	}

	@Override
	public void reset() {
		items.clear();
	}

}
