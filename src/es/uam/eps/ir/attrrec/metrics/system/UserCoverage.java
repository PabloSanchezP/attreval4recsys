/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.metrics.system;

import java.util.Set;
import java.util.TreeSet;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;

/***
 * Metric to analyze the user coverage
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class UserCoverage<U, I> implements SystemMetric<U, I> {

	private Set<U> users;

	public UserCoverage() {
		users = new TreeSet<>();
	}

	@Override
	public void add(Recommendation<U, I> recommendation) {
		users.add(recommendation.getUser());
	}

	@Override
	public double evaluate() {
		return users.size();
	}

	@Override
	public void combine(SystemMetric<U, I> other) {
	}

	@Override
	public void reset() {
		users.clear();
	}

}
