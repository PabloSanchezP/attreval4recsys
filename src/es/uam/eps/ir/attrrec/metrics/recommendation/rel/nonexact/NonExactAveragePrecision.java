/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact;

import java.util.List;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model.IdealNonExactRelevanceModel.UserIdealNonExactRelevanceModel;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model.NonExactRelevanceModel;

/***
 * Non exact Average Precision (computes the average precision of both the
 * relevant items list and the list of the non-relevant ones)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class NonExactAveragePrecision<U, I extends Comparable<I>> extends NonExactGenericMetric<U, I> {

	public NonExactAveragePrecision(int cutoff, NonExactRelevanceModel<U, I> nonExactRelModel) {
		super(cutoff, nonExactRelModel);
	}

	@Override
	public double computeMetric(List<I> topN, List<I> relevantItems, List<I> noRelevantItems,
			UserIdealNonExactRelevanceModel<U, I> userRelModel) {

		double acc = 0;
		double rank = 0;
		double precSum = 0;

		// Relevant Items
		for (I item : topN) {
			rank++;
			if (relevantItems.contains(item)) {
				precSum += userRelModel.gainOfRelevantItem(item);
				acc += precSum / rank;
			}
		}

		rank = 0;
		precSum = 0;
		// No Relevant Items
		for (I item : topN) {
			rank++;
			if (noRelevantItems.contains(item)) {
				precSum += userRelModel.gainOfNotRelevantItem(item);
				acc += precSum / rank;
			}
		}

		return acc / userRelModel.getRelevantItems().size();
	}

}
