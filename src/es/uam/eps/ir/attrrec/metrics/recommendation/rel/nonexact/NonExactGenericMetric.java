/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model.IdealNonExactRelevanceModel;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model.IdealNonExactRelevanceModel.UserIdealNonExactRelevanceModel;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;

/***
 * Non exact generic metric (it will split the top N items between real relevant
 * and no relevant ones)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public abstract class NonExactGenericMetric<U, I> extends AbstractRecommendationMetric<U, I> {

	protected final IdealNonExactRelevanceModel<U, I> nonExactRelModel;
	protected final int cutoff;

	/**
	 * Constructor.
	 *
	 * @param cutoff
	 *            maximum length of recommended lists
	 * @param relModel
	 *            relevance model
	 */
	public NonExactGenericMetric(int cutoff, IdealNonExactRelevanceModel<U, I> nonExactRelModel) {
		this.nonExactRelModel = nonExactRelModel;
		this.cutoff = cutoff;
	}

	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		UserIdealNonExactRelevanceModel<U, I> userRelModel = nonExactRelModel.getModel(recommendation.getUser());

		List<I> topN = recommendation.getItems().stream().limit(cutoff).map(Tuple2od::v1).collect(Collectors.toList());

		List<I> topNRelevantAndRecommendedItems = new ArrayList<>();
		List<I> topNNoRelevantAndRecommendedItems = new ArrayList<>();

		for (I item : topN) {
			if (userRelModel.isRelevant(item)) {
				topNRelevantAndRecommendedItems.add(item);
			} else {
				topNNoRelevantAndRecommendedItems.add(item);
			}
		}

		return computeMetric(topN, topNRelevantAndRecommendedItems, topNNoRelevantAndRecommendedItems, userRelModel);
	}

	/***
	 * Method to compute the specific metric. Need to be override for every child class
	 * @param topN the topN list
	 * @param relevantItems the relevant items
	 * @param noRelevantItems the non relevant items
	 * @param userRelModel the Non exact relevance model
	 * @return the result of he metric
	 */
	public abstract double computeMetric(List<I> topN, List<I> relevantItems, List<I> noRelevantItems,
			UserIdealNonExactRelevanceModel<U, I> userRelModel);

}
