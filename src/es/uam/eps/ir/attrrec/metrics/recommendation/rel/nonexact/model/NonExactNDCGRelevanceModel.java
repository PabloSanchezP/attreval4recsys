/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;

/**
 * Relevance model for nDCG, in which the gains of all relevant documents need
 * to be known for the normalization of the metric.
 *
 * @param <U>
 *            type of the users
 * @param <I>
 *            type of the items
 */
public class NonExactNDCGRelevanceModel<U, I extends Comparable<I>> extends IdealNonExactRelevanceModel<U, I> {

	private final PreferenceData<U, I> testData;
	private final double threshold;
	private final double defaultValueNegativeItem;
	private final ItemSimilarity<I> itemSim;
	private final TreeMap<Double, Double> alphas;

	/**
	 * Constructors.
	 *
	 * @param caching
	 *            are the user relevance models being cached?
	 * @param testData
	 *            test subset of preferences
	 * @param threshold
	 *            relevance threshold
	 */
	public NonExactNDCGRelevanceModel(boolean caching, PreferenceData<U, I> testData, double threshold,
			ItemSimilarity<I> itemSim, double defaultValueNegativeItem, TreeMap<Double, Double> alphas) {
		super(caching, testData.getUsersWithPreferences());
		this.testData = testData;
		this.threshold = threshold;
		this.defaultValueNegativeItem = defaultValueNegativeItem;
		this.itemSim = itemSim;
		this.alphas = alphas;
	}

	/**
	 * @param user
	 *            input user
	 * @return ndcg relevance model for input user
	 */
	@Override
	protected NonExactUserNDCGRelevanceModel get(U user) {
		return new NonExactUserNDCGRelevanceModel(user);
	}

	/**
	 * User relevance model for nDCG.
	 *
	 */
	public class NonExactUserNDCGRelevanceModel
			implements IdealNonExactRelevanceModel.UserIdealNonExactRelevanceModel<U, I> {

		private final Object2DoubleMap<I> unmodifiableGainMap;
		private final Object2DoubleMap<I> modifiableGainMap;

		private LinkedHashSet<I> modifiablefullRelevantItems;

		

		/**
		 * Constructor.
		 *
		 * @param user
		 *            user whose relevance model is computed
		 */
		public NonExactUserNDCGRelevanceModel(U user) {
			this.unmodifiableGainMap = new Object2DoubleOpenHashMap<>();
			unmodifiableGainMap.defaultReturnValue(defaultValueNegativeItem);
			
			this.modifiableGainMap = new Object2DoubleOpenHashMap<>();
			modifiableGainMap.defaultReturnValue(defaultValueNegativeItem);

			List<Tuple2od<I>> lst = testData.getUserPreferences(user).filter(iv -> iv.v2 >= threshold).
					collect(Collectors.toList());
			
			modifiablefullRelevantItems = testItemsOrderedByRating(lst);

			testData.getUserPreferences(user).filter(iv -> iv.v2 >= threshold).forEach(iv -> {
				double gain = Math.pow(2, iv.v2 - threshold + 1.0) - 1.0;
				unmodifiableGainMap.put(iv.v1, gain);
				modifiableGainMap.put(iv.v1, gain);
			});

		}

		/**
		 *
		 *
		 * @return set of relevant items
		 */
		@Override
		public Set<I> getRelevantItems() {
			return unmodifiableGainMap.keySet();
		}

		/**
		 *
		 *
		 * @param item
		 *            input item
		 * @return true is item is relevant, false otherwise
		 */
		@Override
		public boolean isRelevant(I item) {
			return unmodifiableGainMap.containsKey(item);
		}

		/**
		 *
		 *
		 * @param item
		 *            input item
		 * @return relevance gain of the input item
		 */
		@Override
		public double gain(I item) {
			return unmodifiableGainMap.getDouble(item);
		}

		/**
		 * Get the vector of gains of the relevant items.
		 *
		 * @return array of positive relevance gains
		 */
		public double[] getGainValues() {
			return unmodifiableGainMap.values().toDoubleArray();
		}

		@Override
		public double gainOfRelevantItem(I item) {
			double res = 0;
			if (modifiableGainMap.containsKey(item)) {
				res = modifiableGainMap.get(item);
				modifiableGainMap.remove(item);
				modifiablefullRelevantItems.remove(item);
			}
			return res;
		}

		@Override
		public double gainOfNotRelevantItem(I item) {
			double gain = defaultValueNegativeItem;
			double maxSim = 0;
			I itemMostSimilar = null;
			for (I testItem : modifiablefullRelevantItems) {
				if (itemSim.containsItem(item) && itemSim.containsItem(testItem)) {
					double sim = itemSim.similarity(testItem, item);
					if (!Double.isNaN(sim) && sim > 0 && sim > maxSim) {
						maxSim = sim;
						itemMostSimilar = testItem;
					}
				}
			}

			if (itemMostSimilar != null) {
				gain = getAlpha(maxSim) * gainOfRelevantItem(itemMostSimilar);
			}
			return gain;
		}

		protected double getAlpha(double maxSim) {
			double maxKey = 0;
			for (Double key : alphas.keySet()) {
				if (maxSim <= key) {
					return alphas.get(key);
				}
				maxKey = key;
			}
			return alphas.get(maxKey);
		}

		@Override
		public double computeAndDeleteMaxGain(I item) {
			// Check if the item is in the set of relevantItems
			double gain = gainOfRelevantItem(item);
			if (gain != 0) {
				return gain;
			}

			// else, return the gain of not relevant item
			return gainOfNotRelevantItem(item);
		}

	}

}