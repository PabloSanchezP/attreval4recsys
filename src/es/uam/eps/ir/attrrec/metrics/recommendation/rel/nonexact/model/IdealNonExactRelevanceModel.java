/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;

/***
 * Ideal No Relevance Model. Similar to ideal relevance model, but the no
 * relevance model of the user have three methods: -gainOfRelevantItem()
 * -gainOfNotRelevanItems() -computeAndDeleteMaxGain()
 * 
 * All three will remove the relevant items while they are called
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public abstract class IdealNonExactRelevanceModel<U, I> extends RelevanceModel<U, I> {

	/**
	 * Full constructor: allows to specify whether to cache the user relevance
	 * models and for which users.
	 *
	 * @param caching
	 *            are the user relevance models cached?
	 * @param users
	 *            users whose relevance models are cached
	 */
	public IdealNonExactRelevanceModel(boolean caching, Stream<U> users) {
		super(caching, users);
	}

	/**
	 * No caching constructor.
	 */
	public IdealNonExactRelevanceModel() {
		super();
	}

	/**
	 * Caching constructor.
	 *
	 * @param users
	 *            users whose relevance models are cached
	 */
	public IdealNonExactRelevanceModel(Stream<U> users) {
		super(users);
	}

	@Override
	protected abstract UserIdealNonExactRelevanceModel<U, I> get(U user);

	@Override
	public UserIdealNonExactRelevanceModel<U, I> getModel(U user) {
		return (UserIdealNonExactRelevanceModel<U, I>) super.getModel(user);
	}

	/**
	 * User relevance model for IdealRelevanceModel
	 *
	 * @param <U>
	 *            type of the users
	 * @param <I>
	 *            type of the item
	 */
	public interface UserIdealNonExactRelevanceModel<U, I> extends UserRelevanceModel<U, I> {

		/**
		 * Obtains all the items relevant to the user.
		 *
		 * @return set of items relevant to the user
		 */
		public Set<I> getRelevantItems();

		/****
		 * Method to obtain the gain of an item in the test set that it is relevant
		 * 
		 * @param item
		 *            the item
		 * @return the gain
		 */
		public double gainOfRelevantItem(I item);

		/****
		 * Method to obtain the gain of an item in the test set that it is not relevant
		 * 
		 * @param item
		 *            the item
		 * @return the gain
		 */
		public double gainOfNotRelevantItem(I item);

		/***
		 * Method to compute the max gain of an item (relevant or not)
		 * 
		 * @param item
		 *            the item
		 * @return the gain of that item
		 */
		public double computeAndDeleteMaxGain(I item);

	}
	
	/***
	 * Method to order the test set by rating (relevance) and then by id to solve ties
	 * @param lst the list of the test set
	 * @return
	 */
	public static <I extends Comparable<I>> LinkedHashSet<I> testItemsOrderedByRating(List<Tuple2od<I>> lst){
		LinkedHashSet<I> result = new LinkedHashSet<>();
		
		lst.sort(new Comparator<Tuple2od<I>>() {

			@Override
			public int compare(Tuple2od<I> o1, Tuple2od<I> o2) {
				if (o1.v2 == o2.v2) {
					return o1.v1.compareTo(o2.v1);
				}
				
				if (o1.v2 < o2.v2) {
					return -1;
				}					
				else {
					return 1;
				}
				
			}
			
		}.reversed());
		
		
		for (Tuple2od<I> itemt: lst) {
			result.add(itemt.v1);
		}
		
		return result;
		
	}
}