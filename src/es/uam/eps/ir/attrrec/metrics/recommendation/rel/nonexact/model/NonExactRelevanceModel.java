/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity;

/***
 * Non exact relevance model
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class NonExactRelevanceModel<U, I extends Comparable<I>> extends IdealNonExactRelevanceModel<U, I> {

	private final ItemSimilarity<I> itemSim;
	private final PreferenceData<U, I> testData;
	private final double threshold;
	private final double defaultValueNegativeItem;
	private final TreeMap<Double, Double> alphas;

	public NonExactRelevanceModel(boolean caching, PreferenceData<U, I> testData, double threshold,
			ItemSimilarity<I> itemSim, double defaultValueNegativeItem, TreeMap<Double, Double> alphas) {
		super(caching, testData.getUsersWithPreferences());
		this.testData = testData;
		this.threshold = threshold;
		this.itemSim = itemSim;
		this.defaultValueNegativeItem = 0;
		this.alphas = alphas;
	}

	@Override
	protected UserIdealNonExactRelevanceModel<U, I> get(U user) {
		return new UserNonExactRelevanceModel(user);
	}

	public class UserNonExactRelevanceModel implements UserIdealNonExactRelevanceModel<U, I> {

		private final Set<I> unModifiablefullRelevantItems;
		private LinkedHashSet<I> modifiablefullRelevantItems;

		public UserNonExactRelevanceModel(U user) {
			
			List<Tuple2od<I>> lst = testData.getUserPreferences(user).filter(iv -> iv.v2 >= threshold).
					collect(Collectors.toList());
			
			this.unModifiablefullRelevantItems = lst.stream().map(t -> t.v1).collect(Collectors.toSet());		

			modifiablefullRelevantItems = testItemsOrderedByRating(lst);
		}

		@Override
		public Set<I> getRelevantItems() {
			return unModifiablefullRelevantItems;
		}

		@Override
		public boolean isRelevant(I item) {
			return unModifiablefullRelevantItems.contains(item);
		}

		@Override
		public double gain(I item) {
			return isRelevant(item) ? 1.0 : 0.0;
		}

		@Override
		public double computeAndDeleteMaxGain(I item) {

			// Check if the item is in the set of relevantItems
			double gain = gainOfRelevantItem(item);
			if (gain != 0) {
				return gain;
			}

			// else, return the gain of not relevant item
			return gainOfNotRelevantItem(item);
		}

		@Override
		public double gainOfRelevantItem(I item) {
			if (modifiablefullRelevantItems.contains(item)) {
				modifiablefullRelevantItems.remove(item);
				return 1.0;
			}
			return 0;
		}

		@Override
		public double gainOfNotRelevantItem(I item) {
			double gain = defaultValueNegativeItem;
			double maxSim = 0;
			I itemMostSimilar = null;
			for (I testItem : modifiablefullRelevantItems) {
				if (itemSim.containsItem(item) && itemSim.containsItem(testItem)) {
					double sim = itemSim.similarity(testItem, item);
					if (!Double.isNaN(sim) && sim > 0 && sim > maxSim) {
						maxSim = sim;
						itemMostSimilar = testItem;
					}
				}
			}

			if (itemMostSimilar != null) {
				gain = getAlpha(maxSim);
				modifiablefullRelevantItems.remove(itemMostSimilar);
			}
			return gain;
		}

		protected double getAlpha(double maxSim) {
			double maxKey = 0;
			for (Double key : alphas.keySet()) {
				if (maxSim <= key) {
					return alphas.get(key);
				}
				maxKey = key;
			}
			return alphas.get(maxKey);
		}
		
		

	}

}
