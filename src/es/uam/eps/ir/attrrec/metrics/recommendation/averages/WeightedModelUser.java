/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.metrics.recommendation.averages;

import java.util.Optional;

import org.jooq.lambda.tuple.Tuple2;

import es.uam.eps.ir.attrrec.datamodel.feature.UserFeatureData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;

public class WeightedModelUser<U, I, F, V> {

	public enum UserMetricWeight {
		RANDOM("RND"), NORMAL("NRM"), USER_FEATURE_BINARY("UFB"), USER_FEATURE_PROPORTIONAL("UFP"), PROPORTIONAL_TRAIN(
				"PROT"), INVERSE_PROPORTIONAL_TRAIN("IPROT");

		private String shortName;

		private UserMetricWeight(String name) {
			this.shortName = name;
		}

		public String getShortName() {
			return shortName;
		}
	};

	private final PreferenceData<U, I> preferenceDataTrain;
	private final UserMetricWeight weight;
	private final UserFeatureData<U, F, V> usersFeatures;
	private final F featureUser;

	public WeightedModelUser(PreferenceData<U, I> data, UserMetricWeight weight) {
		this(data, weight, null, null);
	}

	// Constructor when using the features of the users
	public WeightedModelUser(PreferenceData<U, I> data, UserMetricWeight weight, F featureSelected,
			UserFeatureData<U, F, V> usersFeatures) {
		this.preferenceDataTrain = data;
		this.weight = weight;
		this.usersFeatures = usersFeatures;
		this.featureUser = featureSelected;
	}

	public double getWeight(U user) {
		switch (weight) {
		case PROPORTIONAL_TRAIN:
			return preferenceDataTrain.getUserPreferences(user).count();
		case INVERSE_PROPORTIONAL_TRAIN:
			long numbersRated = preferenceDataTrain.getUserPreferences(user).count();
			// Avoid dividing by 0 if the user does not have anything in the training set
			return numbersRated == 0 ? 1.0 : 1.0 / numbersRated;
		case RANDOM:
			return Math.random();
		case USER_FEATURE_BINARY:
			return usersFeatures.getUserFeatures(user).filter(t -> t.v1.equals(featureUser)).findFirst().isPresent()
					? 1.0
					: 0;
		case USER_FEATURE_PROPORTIONAL:
			Optional<Tuple2<F, V>> t = usersFeatures.getUserFeatures(user).findFirst();
			if (t.isPresent()) {
				return usersFeatures.getFeatureUsers(t.get().v1).count();
			} else
				return 0;

		default:
			return 1.0;
		}

	}

}