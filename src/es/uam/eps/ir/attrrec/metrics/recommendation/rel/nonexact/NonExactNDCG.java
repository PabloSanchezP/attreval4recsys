/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact;

import java.util.Arrays;
import java.util.List;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model.IdealNonExactRelevanceModel;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model.IdealNonExactRelevanceModel.UserIdealNonExactRelevanceModel;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model.NonExactNDCGRelevanceModel;
import es.uam.eps.ir.attrrec.metrics.recommendation.rel.nonexact.model.NonExactNDCGRelevanceModel.NonExactUserNDCGRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rank.LogarithmicDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;


/**
 * Non exact version for NDCG
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class NonExactNDCG<U, I extends Comparable<I>> extends NonExactGenericMetric<U, I> {
	private final RankingDiscountModel disc;

	public NonExactNDCG(int cutoff, IdealNonExactRelevanceModel<U, I> nonExactRelModel) {
		super(cutoff, nonExactRelModel);
		this.disc = new LogarithmicDiscountModel();
	}

	@Override
	public double computeMetric(List<I> topN, List<I> relevantItems, List<I> noRelevantItems,
			UserIdealNonExactRelevanceModel<U, I> userRelModel) {

		double ndcg = 0.0;
		int rank = 0;

		// First, items that are in the test set
		for (I item : topN) {
			if (relevantItems.contains(item)) {
				ndcg += userRelModel.gainOfRelevantItem(item) * disc.disc(rank);
			}
			rank++;
		}
		rank = 0;
		for (I item : topN) {
			if (noRelevantItems.contains(item)) {
				ndcg += userRelModel.gainOfNotRelevantItem(item) * disc.disc(rank);
			}
			rank++;
		}

		if (ndcg > 0) {
			ndcg /= idcg((NonExactUserNDCGRelevanceModel) userRelModel);
		}

		return ndcg;

	}

	private double idcg(NonExactNDCGRelevanceModel<U,I>.NonExactUserNDCGRelevanceModel relModel) {
		double[] gains = relModel.getGainValues();
		Arrays.sort(gains);

		double idcg = 0;
		int n = Math.min(cutoff, gains.length);
		int m = gains.length;

		for (int rank = 0; rank < n; rank++) {
			idcg += gains[m - rank - 1] * disc.disc(rank);
		}

		return idcg;
	}

	

}
