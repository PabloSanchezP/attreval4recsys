/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.metrics.recommendation.averages;


import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.AbstractSystemMetric;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;
import es.uam.eps.ir.ranksys.metrics.rel.IdealRelevanceModel;

/**
 * Average of a recommendation metric: system metric based on the weighted
 * arithmetic mean of a recommendation metric for a set of users'
 * recommendations. The higher the number of preferences of a user in the train
 * set, the higher the weight of that user
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 *            type of the users
 * @param <I>
 *            type of the items
 */
public class WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<U, I, F, V>
		extends AbstractSystemMetric<U, I> {

	private final RecommendationMetric<U, I> metric;
	private double sum;
	private double totalWeight;

	private final WeightedModelUser<U, I, F, V> weightedModelUser;
	private final IdealRelevanceModel<U, I> relModel;

	/**
	 * Constructor in which the number of users of the recommendation metric to be
	 * averaged is specified. Recommendations returning NaN or missing
	 * recommendations are treated as zeros to the average.
	 *
	 * @param metric
	 *            recommendation metric to be averaged
	 * @param numUsers
	 *            number of expected users' recommendations
	 */
	public WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs(RecommendationMetric<U, I> metric,
			IdealRelevanceModel<U, I> relModel, WeightedModelUser<U, I, F, V> modelUser) {
		this.metric = metric;
		this.sum = 0;
		this.totalWeight = 0;
		this.relModel = relModel;
		this.weightedModelUser = modelUser;
	}

	/**
	 * Adds the recommendation metric to the average and returns the user value.
	 *
	 * @param recommendation
	 *            recommendation to be added
	 * @return results of the recommender metric
	 */
	public double addAndEvaluate(Recommendation<U, I> recommendation) {
		double weight = 0;
		U user = recommendation.getUser();

		boolean isRelevant = !(this.relModel.getModel(user).getRelevantItems().isEmpty()); // If it is empty, it is not
																							// relevant
		weight = weightedModelUser.getWeight(user);

		if (isRelevant && weight > 0.0) {
			double v = metric.evaluate(recommendation);
			if (!Double.isNaN(v)) {
				sum += v * weight;
				totalWeight += weight;
			} else {
				System.out.println(
						"User: " + user + " is relevant but metric " + metric.toString() + " evaluation is NaN");
			}
			return v * weight;
		}

		return 0;
	}

	@Override
	public void add(Recommendation<U, I> recommendation) {
		addAndEvaluate(recommendation);
	}

	@Override
	public void combine(SystemMetric<U, I> other) {
		sum += ((WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<U, I, F, V>) other).sum;

		totalWeight += ((WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<U, I, F, V>) other).totalWeight;
	}

	@Override
	public double evaluate() {
		return sum / totalWeight;
	}

	@Override
	public void reset() {
		this.sum = 0;
		this.totalWeight = 0;
	}

}
