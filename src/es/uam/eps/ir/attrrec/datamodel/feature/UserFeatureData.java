/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.datamodel.feature;

import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;

import es.uam.eps.ir.ranksys.core.index.FeatureIndex;
import es.uam.eps.ir.ranksys.core.index.UserIndex;

/**
 * User-feature data.
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 * 
 * @param <I>
 *            type of the users
 * @param <F>
 *            type of the features
 * @param <V>
 *            type of the information about user-feature pairs
 */
public interface UserFeatureData<U, F, V> extends UserIndex<U>, FeatureIndex<F> {

	/**
	 * Returns the number of users with features.
	 *
	 * @return number of users with features
	 */
	int numUsersWithFeatures();

	/**
	 * Returns the number of features with users.
	 *
	 * @return number of features with users
	 */
	int numFeaturesWithUsers();

	/**
	 * Returns a stream of users with features.
	 *
	 * @return stream of users with features
	 */
	Stream<U> getUsersWithFeatures();

	/**
	 * Returns a stream of features with users.
	 *
	 * @return stream of features with users.
	 */
	Stream<F> getFeaturesWithUsers();

	/**
	 * Returns a stream of users with the feature.
	 *
	 * @param f
	 *            feature
	 * @return stream of users with the feature.
	 */
	Stream<Tuple2<U, V>> getFeatureUsers(final F f);

	/**
	 * Returns a stream of features of the user.
	 *
	 * @param u
	 *            user
	 * @return stream of features of the user.
	 */
	Stream<Tuple2<F, V>> getUserFeatures(final U u);

	/**
	 * Returns the number of features of the user.
	 *
	 * @param u
	 *            user
	 * @return number of features of the user
	 */
	int numFeatures(U u);

	/**
	 * Returns the number of users with the feature.
	 *
	 * @param f
	 *            feature
	 * @return number of users with the feature
	 */
	int numUsers(F f);

}
