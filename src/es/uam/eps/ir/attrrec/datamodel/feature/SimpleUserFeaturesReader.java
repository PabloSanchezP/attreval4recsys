/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.datamodel.feature;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple;
import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.formats.parsing.Parser;

/**
 * Reads a file of tab-separated user-feat-value triples, one per line.
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 */
public class SimpleUserFeaturesReader implements UserFeaturesReader {

	/**
	 * Returns and instance of this class.
	 *
	 * @param <U>
	 *            user type
	 * @param <F>
	 *            feat type
	 * @return an instance of SimpleFeaturesReader
	 */
	public static <U, F> SimpleUserFeaturesReader get() {
		return new SimpleUserFeaturesReader();
	}

	private SimpleUserFeaturesReader() {
	}

	@Override
	public <U, F> Stream<Tuple3<U, F, Double>> read(InputStream in, Parser<U> up, Parser<F> fp) throws IOException {
		return new BufferedReader(new InputStreamReader(in)).lines().map(line -> {
			String[] tokens = line.split("\t", 3);
			U item = up.parse(tokens[0]);
			F feat = fp.parse(tokens[1]);

			return Tuple.tuple(item, feat, 1.0);
		});
	}

}
