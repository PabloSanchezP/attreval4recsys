/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.datamodel.feature;

import static org.jooq.lambda.tuple.Tuple.tuple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;

/**
 * Simple map-based user feature data (user and list of features and features and list of users)
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <I>
 *            type of the users
 * @param <F>
 *            type of the features
 * @param <V>
 *            type of the information about user-feature pairs
 */
public class SimpleUserFeatureData<U, F, V> implements UserFeatureData<U, F, V> {

	private final Map<U, List<Tuple2<F, V>>> userMap;
	private final Map<F, List<Tuple2<U, V>>> featMap;

	protected SimpleUserFeatureData(Map<U, List<Tuple2<F, V>>> userMap, Map<F, List<Tuple2<U, V>>> featMap) {
		this.userMap = userMap;
		this.featMap = featMap;
	}

	@Override
	public int numUsersWithFeatures() {
		return userMap.size();
	}

	@Override
	public int numFeaturesWithUsers() {
		return featMap.size();
	}

	@Override
	public Stream<U> getUsersWithFeatures() {
		return userMap.keySet().stream();
	}

	@Override
	public Stream<F> getFeaturesWithUsers() {
		return featMap.keySet().stream();
	}

	@Override
	public Stream<Tuple2<U, V>> getFeatureUsers(F f) {
		return featMap.getOrDefault(f, new ArrayList<>()).stream();
	}

	@Override
	public Stream<Tuple2<F, V>> getUserFeatures(U u) {
		return userMap.getOrDefault(u, new ArrayList<>()).stream();
	}

	@Override
	public int numFeatures(U u) {
		return userMap.getOrDefault(u, new ArrayList<>()).size();
	}

	@Override
	public int numUsers(F f) {
		return featMap.getOrDefault(f, new ArrayList<>()).size();
	}

	@Override
	public boolean containsUser(U u) {
		return userMap.containsKey(u);
	}

	@Override
	public int numUsers() {
		return userMap.size();
	}

	@Override
	public Stream<U> getAllUsers() {
		return userMap.keySet().stream();
	}

	@Override
	public boolean containsFeature(F f) {
		return featMap.containsKey(f);
	}

	@Override
	public int numFeatures() {
		return featMap.size();
	}

	@Override
	public Stream<F> getAllFeatures() {
		return featMap.keySet().stream();
	}

	/**
	 * Loads an instance of the class from a stream of triples.
	 *
	 * @param <U>
	 *            type of user
	 * @param <F>
	 *            type of feat
	 * @param <V>
	 *            type of value
	 * @param tuples
	 *            stream of item-feat-value triples
	 * @return a feature data object
	 */
	public static <U, F, V> SimpleUserFeatureData<U, F, V> load(Stream<Tuple3<U, F, V>> tuples) {
		Map<U, List<Tuple2<F, V>>> userMap = new HashMap<>();
		Map<F, List<Tuple2<U, V>>> featMap = new HashMap<>();

		tuples.forEach(t -> {
			userMap.computeIfAbsent(t.v1, v1 -> new ArrayList<>()).add(tuple(t.v2, t.v3));
			featMap.computeIfAbsent(t.v2, v2 -> new ArrayList<>()).add(tuple(t.v1, t.v3));
		});

		return new SimpleUserFeatureData<>(userMap, featMap);
	}

}
