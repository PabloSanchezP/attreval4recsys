/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.datamodel.feature;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.formats.parsing.Parser;

public interface UserFeaturesReader {

	/**
	 * Reads user-feature-value triples from a file.
	 *
	 * @param <U>
	 *            user type
	 * @param <F>
	 *            feat type
	 * @param in
	 *            path to file
	 * @param up
	 *            user parser
	 * @param fp
	 *            feat parser
	 * @return stream of user-feature-value triples
	 * @throws IOException
	 *             when I/O problems
	 */
	public default <U, F> Stream<Tuple3<U, F, Double>> read(String in, Parser<U> up, Parser<F> fp) throws IOException {
		return read(new FileInputStream(in), up, fp);

	}

	/**
	 * Reads user-feature-value triples from an input stream.
	 *
	 * @param <U>
	 *            user type
	 * @param <F>
	 *            feat type
	 * @param in
	 *            input stream to read
	 * @param up
	 *            item parser
	 * @param fp
	 *            feat parser
	 * @return stream of user-feature-value triples
	 * @throws IOException
	 *             when I/O problems
	 */
	public <U, F> Stream<Tuple3<U, F, Double>> read(InputStream in, Parser<U> up, Parser<F> fp) throws IOException;

}
