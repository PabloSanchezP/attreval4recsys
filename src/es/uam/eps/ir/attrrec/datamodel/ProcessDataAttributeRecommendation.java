/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.attrrec.datamodel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import es.uam.eps.ir.ranksys.core.preference.PreferenceData;

/***
 * Class containing specific methods to process data for attribute recommendation 
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class ProcessDataAttributeRecommendation {

	/***
	 * Method that will receive am user feature file (containing more than one
	 * feature) and will obtain a set of user features files
	 * 
	 * @param inputFeatureFile
	 *            the input user file
	 * @param outputFeatureFiles
	 */
	public static void splitFeatureUserFile(String inputFeatureFile, String[] outputFeatureFiles) {
		try {
			List<PrintStream> ptrs = new ArrayList<>();
			Stream<String> stream = Files.lines(Paths.get(inputFeatureFile));
			for (String destPath : outputFeatureFiles) {
				ptrs.add(new PrintStream(destPath));
			}
			stream.forEach(line -> {
				String data[] = line.split("\t");

				// data [0] is the user
				for (int i = 0; i < outputFeatureFiles.length; i++) {
					ptrs.get(i).println(data[0] + "\t" + data[i + 1]);
				}

			});
			stream.close();
			for (PrintStream ptr : ptrs) {
				ptr.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/***
	 * Method to parse the feature file of movielens to a format that can be used in
	 * ranksys
	 * 
	 * @param featureFile
	 *            the feature file of movielens
	 * @param outputResultFile
	 *            the output file
	 * @param outputfeatureMap
	 *            the feature map
	 */
	public static void parseFeaturesMovielens(String moviesInfoFiles, String outputResultFile,
			String outputfeatureMap) {
		AtomicInteger counter = new AtomicInteger(0);
		Map<String, Integer> featuresIndex = new HashMap<>();
		try {
			PrintStream outputResultFilePS = new PrintStream(outputResultFile);
			PrintStream outputfeatureMapPS = new PrintStream(outputfeatureMap);

			Stream<String> stream = Files.lines(Paths.get(moviesInfoFiles));
			stream.forEach(line -> {
				String data[] = line.split("::");
				String genres[] = data[2].split("\\|");

				for (String genre : genres) {
					if (featuresIndex.get(genre) == null) {
						featuresIndex.put(genre, counter.incrementAndGet());
					}
					outputResultFilePS.println(data[0] + "\t" + featuresIndex.get(genre));
				}
			});
			outputResultFilePS.close();
			stream.close();

			for (Map.Entry<String, Integer> entry : featuresIndex.entrySet()) {
				outputfeatureMapPS.println(entry.getKey() + "\t" + entry.getValue());
			}
			outputfeatureMapPS.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/***
	 * Method to group the user age into an interval of ages
	 * 
	 * @param featureUserFile
	 *            the feature user file
	 * @param outputUserFile
	 *            the output user file with the new ages
	 * @param agesOrdered
	 *            the set of ages
	 */
	public static void groupUserAges(String featureUserFile, String outputUserFile, TreeSet<Integer> agesOrdered) {
		Stream<String> stream;
		try {
			PrintStream outStream = new PrintStream(outputUserFile);
			stream = Files.lines(Paths.get(featureUserFile));
			stream.forEach(line -> {
				String data[] = line.split("\t");
				String user = data[0];
				try {
					Integer age = Integer.parseInt(data[1]);
					Integer newAge = agesOrdered.floor(age);
					if (newAge != null) {
						outStream.println(user + "\t" + newAge);
					}
				} catch (NumberFormatException exception) {

				}

			});
			stream.close();
			outStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static <U, I> void markUsersByNumberPreferencesQuartiles(PreferenceData<U, I> prefData, String outputFile) {
		try {
			PrintStream outPutResult = new PrintStream(outputFile);
			List<Number> lstPreferences = new ArrayList<>();
			prefData.getUsersWithPreferences().forEach(u -> {
				lstPreferences.add(prefData.getUserPreferences(u).count());
			});

			List<Number> quartiles = getQuartiles(lstPreferences);
			System.out.println("Quartiles: " + quartiles);
			prefData.getUsersWithPreferences().forEach(u -> {
				long prefsUser = prefData.getUserPreferences(u).count();
				// First
				if (prefsUser <= quartiles.get(0).longValue()) {
					outPutResult.println(u + "\t" + "Q1");
				} else {
					if (prefsUser <= quartiles.get(1).longValue()) {
						outPutResult.println(u + "\t" + "Q2");
					} else {
						if (prefsUser <= quartiles.get(2).longValue()) {
							outPutResult.println(u + "\t" + "Q3");
						} else {
							outPutResult.println(u + "\t" + "Q4");
						}
					}

				}

			});
			outPutResult.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<Number> getQuartiles(List<Number> lstNumbers) {
		List<Number> result = new ArrayList<>();
		Number q2 = getMedian(lstNumbers);
		List<Number> prevQ2 = new ArrayList<>();
		List<Number> postQ2 = new ArrayList<>();
		for (Number n : lstNumbers) {
			if (n.doubleValue() == q2.doubleValue()) {
				postQ2.add(n);
				prevQ2.add(n);
			} else {
				if (n.doubleValue() > q2.doubleValue()) {
					postQ2.add(n);
				} else {
					prevQ2.add(n);
				}
			}
		}
		Number q1 = getMedian(prevQ2);
		Number q3 = getMedian(postQ2);
		result.add(q1);
		result.add(q2);
		result.add(q3);
		return result;
	}

	public static Number getMedian(List<Number> lstNumbers) {
		if (lstNumbers.size() == 0) {
			return Double.NaN;
		}

		Number[] toArr = new Number[lstNumbers.size()];
		toArr = lstNumbers.toArray(toArr);
		Arrays.sort(toArr);
		if (toArr.length % 2 == 0) {
			return (toArr[toArr.length / 2].doubleValue() + toArr[toArr.length / 2 - 1].doubleValue()) / 2.0;
		} else {
			return toArr[toArr.length / 2];
		}

	}

	

	/***
	 * Method to perform a simple random split
	 * 
	 * @param source
	 *            the source file
	 * @param destTrain
	 *            the destination train
	 * @param destTest
	 *            the destination test
	 * @param percentajeTrain
	 *            the percentage of ratings that will go to the train and the test
	 *            set
	 */
	public static void randomSplit(String source, String destTrain, String destTest, double percentageTrain,
			long seed) {
		try {
			PrintStream resultFile1 = new PrintStream(destTrain);
			PrintStream resultFile2 = new PrintStream(destTest);

			Stream<String> stream = Files.lines(Paths.get(source));
			Random n = new Random();
			n.setSeed(seed);
			stream.forEach(line -> {
				if (n.nextFloat() > percentageTrain) {
					resultFile2.println(line);
				} else {
					resultFile1.println(line);
				}
			});
			stream.close();
			resultFile1.close();
			resultFile2.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
