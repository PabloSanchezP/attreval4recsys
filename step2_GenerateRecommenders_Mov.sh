#!/bin/bash
#Necessary to execute with ./

: '
  Script for Movielens dataset. It generates the recommenders and
  compute the evaluations
'

#Global variables for jvm
JAR=target/AttrEval4RecSys-0.2.1-SNAPSHOT.jar
jvm_memory=-Xmx30G
train_test_files=TrainTest
java_command=java


rec_prefix=rec
sim_prefix=sim


# General parameters of recommendations
items_recommended=100
rec_strats="T_I" # T_I is train items
train_test_files=TrainTest
dataset_speficic="Mov1M"
cutoffs="5,10,20"


original_datasets=OriginalDatasets
feature_file=$original_datasets/ml-1m/FeaturesMovielens.txt
train_test_files_mov1M=TrainTestMov1M


# Specific parameters of recommendations
# k-NN approaches
all_neighbours="40 60 80 100 120"

# HKV
k_factors="10 50 100"
lambda_factorizers="0.1 1 10"
alpha_factorizers="0.1 1 10"



mymedialite_path=MyMediaLite-3.11/bin
bpr_factors=$k_factors
bpr_bias_regs="0 0.5 1"
bpr_learn_rate=0.05
bpr_iter=50
bpr_regs_u="0.0025 0.001 0.005 0.01 0.1"
extension_Mymed=MyMedLt



for rec_strat in $rec_strats
do
  for dataset in $dataset_speficic
  do
    train_file=$train_test_files"$dataset"/"$dataset"Rnd8020Train.txt
    test_file=$train_test_files"$dataset"/"$dataset"Rnd8020Test.txt

    recommendation_folder="RecommendationFolder""$dataset"
    mkdir -p $recommendation_folder


    # Generate the CB-similarity with both sources of information
    alpha_fst_feature="0.8"
    alpha_snd_feature="0.6"
    file_feature_fst=Mov1MFullDirectors.txt
    file_feature_snd=FeaturesMovielens.txt

    output_sim_file=$train_test_files"$dataset"/SimComposedHierarchicalJaccardDirectorGenresAlphas"$alpha_fst_feature""$alpha_snd_feature".txt
    $java_command $jvm_memory -jar $JAR -o saveComposedSimilarity -trf $train_file -cs 0 -orf $output_sim_file -aFactorizer "$alpha_fst_feature","$alpha_snd_feature" -ff $original_datasets/ml-1m/$file_feature_fst","$original_datasets/ml-1m/$file_feature_snd

    # Now, each feature separated (directors and genres)
    alpha_fst_feature="0.8"
    file_feature_fst=Mov1MFullDirectors.txt
    output_sim_file=$train_test_files"$dataset"/SimComposedHierarchicalJaccardDirectorAlpha"$alpha_fst_feature".txt
    $java_command $jvm_memory -jar $JAR -o saveComposedSimilarity -trf $train_file -cs 0 -orf $output_sim_file -aFactorizer "$alpha_fst_feature" -ff $original_datasets/ml-1m/$file_feature_fst


    alpha_snd_feature="0.6"
    file_feature_snd=FeaturesMovielens.txt
    output_sim_file=$train_test_files"$dataset"/SimComposedHierarchicalJaccardGenresAlpha"$alpha_snd_feature".txt
    $java_command $jvm_memory -jar $JAR -o saveComposedSimilarity -trf $train_file -cs 0 -orf $output_sim_file -aFactorizer $alpha_snd_feature -ff $original_datasets/ml-1m/$file_feature_snd


    output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_"skylineRelevanceRecommender"_"$rec_strat".txt
    $java_command $jvm_memory -jar $JAR -o skylineRecommenders -trf $train_file -tsf $test_file -cIndex true -rr "skylineRelevanceRecommender" -rs "notUsed" -nI $items_recommended -n 20 -orf $output_rec_file -recStrat $rec_strat -thr 4


    for ranksys_recommender in PopularityRecommender RandomRecommender
    do
      output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_"$ranksys_recommender"_"$rec_strat".txt
      $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex true -rr "$ranksys_recommender" -rs "notUsed" -nI $items_recommended -n 20 -orf $output_rec_file -recStrat $rec_strat
    done # End pop and Rnd
    wait



    # for all neighbours

    for neighbours in $all_neighbours
    do

      #Ranksys with similarities UserBased

      for UB_sim in SetJaccardUserSimilarity VectorCosineUserSimilarity
      do
        output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_UB_"$UB_sim"_k"$neighbours"_"$rec_strat".txt
        $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex false -rr UserNeighborhoodRecommender -rs $UB_sim -nI $items_recommended -n $neighbours -orf $output_rec_file -recStrat $rec_strat


      done # End UB sim
      wait


      for IB_sim in SetJaccardItemSimilarity VectorCosineItemSimilarity
      do
        output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_IB_"$IB_sim"_k"$neighbours"_"$rec_strat".txt
        $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex false -rr ItemNeighborhoodRecommender -rs $IB_sim -nI $items_recommended -n $neighbours -orf $output_rec_file -recStrat $rec_strat

      done
      wait #IB Sim


      output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksys_UBContentBasedRecommender_k"$neighbours"_"$rec_strat".txt
      $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex false -rr UBContentBasedRecommender -ff2 $feature_file -nI $items_recommended -n $neighbours -orf $output_rec_file -recStrat $rec_strat



    done # End neigh
    wait




    # Pure CB
    output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_ranksysBaselineCB_"$rec_strat".txt
    $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex false -rr BaselineCB -ff2 $feature_file -nI $items_recommended -n $neighbours -orf $output_rec_file -recStrat $rec_strat



    for recommender in MFRecommenderHKV
    do
      for k_factor in $k_factors
      do
        for lambda_val in $lambda_factorizers
        do
          for alpha_val in $alpha_factorizers
          do
            #Neighbours is put to 20 because this recommender does not use it
            output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_"$recommender"_kFactor"$k_factor"_a"$alpha_val"_l"$lambda_val"_"$rec_strat".txt
            $java_command $jvm_memory -jar $JAR -o ranksysOnlyComplete -trf $train_file -tsf $test_file -cIndex false -rr $recommender -rs "notUsed" -nI $items_recommended -n 20 -orf $output_rec_file -kFactorizer $k_factor -aFactorizer $alpha_val -lFactorizer $lambda_val -recStrat $rec_strat
          done
          wait #End alpha values
        done
        wait #End lambda
      done
      wait #End KFactor
    done
    wait #End RankRecommender


    for repetition in 1 #2 3 4
    do

      for bpr_factor in $bpr_factors
      do
        for bpr_bias_reg in $bpr_bias_regs
        do
          for bpr_reg_U in $bpr_regs_u #Regularization for items and users is the same
          do
            bpr_reg_J=$(echo "$bpr_reg_U/10" | bc -l)

            output_rec_file=$recommendation_folder/"$rec_prefix"_"$dataset"_"$extension_Mymed"_BPRMF_nFact"$bpr_factor"_nIter"$bpr_iter"_LearnR"$bpr_learn_rate"_BiasR"$bpr_bias_reg"_RegU"$bpr_reg_U"_RegI"$bpr_reg_U"_RegJ"$bpr_reg_J""Rep$repetition"_"$rec_strat".txt
            echo $output_rec_file
            output_rec_file2=$output_rec_file"Aux".txt
            if [ ! -f *"$output_rec_file"* ]; then
              if [[ $rec_strat == *T_I* ]]; then
                echo "./$mymedialite_path/item_recommendation --training-file=$train_file --recommender=BPRMF --prediction-file=$output_rec_file2 --predict-items-number=$items_recommended --recommender-options="num_factors=$bpr_factor bias_reg=$bpr_bias_reg reg_u=$bpr_reg_U reg_i=$bpr_reg_U reg_j=$bpr_reg_J learn_rate=$bpr_learn_rate UniformUserSampling=false WithReplacement=false num_iter=$bpr_iter""
                ./$mymedialite_path/item_recommendation --training-file=$train_file --recommender=BPRMF --prediction-file=$output_rec_file2 --predict-items-number=$items_recommended --recommender-options="num_factors=$bpr_factor bias_reg=$bpr_bias_reg reg_u=$bpr_reg_U reg_i=$bpr_reg_U reg_j=$bpr_reg_J learn_rate=$bpr_learn_rate UniformUserSampling=false WithReplacement=false num_iter=$bpr_iter"
                $java_command $jvm_memory -jar $JAR -o ParseMyMediaLite -trf $output_rec_file2 $test_file $output_rec_file
                rm $output_rec_file2
              else
                echo "./$mymedialite_path/item_recommendation --training-file=$train_file --recommender=BPRMF --prediction-file=$output_rec_file2 --predict-items-number=$items_recommended --repeated-items --recommender-options="num_factors=$bpr_factor bias_reg=$bpr_bias_reg reg_u=$bpr_reg_U reg_i=$bpr_reg_U reg_j=$bpr_reg_J learn_rate=$bpr_learn_rate UniformUserSampling=false WithReplacement=false num_iter=$bpr_iter""
                ./$mymedialite_path/item_recommendation --training-file=$train_file --recommender=BPRMF --prediction-file=$output_rec_file2 --predict-items-number=$items_recommended --repeated-items --recommender-options="num_factors=$bpr_factor bias_reg=$bpr_bias_reg reg_u=$bpr_reg_U reg_i=$bpr_reg_U reg_j=$bpr_reg_J learn_rate=$bpr_learn_rate UniformUserSampling=false WithReplacement=false num_iter=$bpr_iter"
                $java_command $jvm_memory -jar $JAR -o ParseMyMediaLite -trf $output_rec_file2 $test_file $output_rec_file
                rm $output_rec_file2
              fi
            fi
          done # Reg U
          wait
        done # Bias reg
        wait
      done # Factors
      wait

    done # Repetition
    wait


  done # End train test files
  wait
done # End recommendationStrategy splits
wait


: '
Evaluation part
'
acc_prefix=naeval
evthreshold=4
for dataset in $dataset_speficic
do
  train_file=$train_test_files"$dataset"/"$dataset"Rnd8020Train.txt
  test_file=$train_test_files"$dataset"/"$dataset"Rnd8020Test.txt

  results_Folder=resultsFolder"$dataset"

  mkdir -p $results_Folder

  #Find all recommenders and compute the ranksysEvaluation

  recommendation_Folder=RecommendationFolder"$dataset"

  find $recommendation_Folder/ -name "$rec_prefix"* | while read recFile; do
    rec_FileName=$(basename "$recFile" .txt) #extension removed


    if [[ $rec_FileName == *_* ]]; then
      for evthreshold in 4 # we work with implicit data
      do

        # Normal evaluations. All users
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false
        fi

        # Non exact evaluation using directors and genres combined
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"NonExactComposed.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -nonEM true -rs $train_test_files"$dataset"/SimComposedHierarchicalJaccardDirectorGenresAlphas0.80.6.txt -cs 0
        fi

        # Non exact evaluation using only directors
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"NonExactComposedDirectorsOnly.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -nonEM true -rs $train_test_files"$dataset"/SimComposedHierarchicalJaccardDirectorAlpha0.8.txt -cs 0
        fi

        # Non exact evaluation using only genres
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"NonExactComposedGenresOnly.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -nonEM true -rs $train_test_files"$dataset"/SimComposedHierarchicalJaccardGenresAlpha0.6.txt -cs 0
        fi


        # Only for users defined as Women
        user_feature=F
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $original_datasets/ml-1m/movUserGenres.txt -ufs $user_feature -compUF true
        fi

        # Only for users defined as Men
        user_feature=M
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $original_datasets/ml-1m/movUserGenres.txt -ufs $user_feature -compUF true
        fi
        # AGES

        # Only for users in the group 1 (1 - 17 years)
        user_feature=1
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $original_datasets/ml-1m/movUserAgeNewIntervals.txt -ufs $user_feature -compUF true
        fi

        # Only for users in the group 2 (18 - 35 years)
        user_feature=18
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $original_datasets/ml-1m/movUserAgeNewIntervals.txt -ufs $user_feature -compUF true
        fi

        # Only for users in the group 3 (35 - 65 years)
        user_feature=35
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $original_datasets/ml-1m/movUserAgeNewIntervals.txt -ufs $user_feature -compUF true
        fi

        # Only for users in the group 3 (65 - ? years)
        user_feature=56
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature.txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $original_datasets/ml-1m/movUserAgeNewIntervals.txt -ufs $user_feature -compUF true
        fi

        # COLD START  (TRAIN)

        # Only for Q1
        user_feature=Q1
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Train".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/Mov1MRnd8020TrainPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q2
        user_feature=Q2
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Train".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/Mov1MRnd8020TrainPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q3
        user_feature=Q3
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Train".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/Mov1MRnd8020TrainPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q4
        user_feature=Q4
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Train".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/Mov1MRnd8020TrainPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # COLD START  (TEST)

        # Only for Q1
        user_feature=Q1
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Test".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/Mov1MRnd8020TestPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q2
        user_feature=Q2
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Test".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/Mov1MRnd8020TestPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q3
        user_feature=Q3
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Test".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/Mov1MRnd8020TestPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi

        # Only for Q4
        user_feature=Q4
        output_result_file=$results_Folder/"$acc_prefix"_EvTh"$evthreshold"_"$rec_FileName"_C$cutoffs"_UF$user_feature"Test".txt"
        if [ ! -f "$output_result_file" ]; then
          echo 'train file is ' $train_file
          #NormalEv
          $java_command $jvm_memory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $train_file -tsf $test_file -rf $recFile -ff $feature_file -thr $evthreshold -rc $cutoffs -orf $output_result_file -onlyAcc false -uff $train_test_files"$dataset"/Mov1MRnd8020TestPrefsQuartiles.txt -ufs $user_feature -compUF true
        fi



      done
      wait
    fi
  done
  wait
done # End dataset
wait
